package com.example.welcome.chatprojectinmvpmodel;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.InstrumentationTestCase;
import android.test.mock.MockContext;
import android.util.Log;

import com.example.welcome.chatprojectinmvpmodel.Activity.Storingactivity;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model.storagemodel;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.storageview;
import com.example.welcome.chatprojectinmvpmodel.Models.ChatMessage;
import com.example.welcome.chatprojectinmvpmodel.sqlitefiles.Myopenhelper;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static com.example.welcome.chatprojectinmvpmodel.sqlitefiles.Myopenhelper.DICTIONARY_TABLE_NAME;
import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 *
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest extends InstrumentationTestCase implements storageview {

    private Myopenhelper mMyopenhelper;

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.example.welcome.chatprojectinmvpmodel", appContext.getPackageName());
    }
    Context context;

    public void setUp() throws Exception {
        super.setUp();


        context = new MockContext();
        storagemodel mstoragemodel = new storagemodel(this);

        mstoragemodel.getvaluesfromdb(context);

        mMyopenhelper = new Myopenhelper(context);


    }

    public void testSomething() {

        assertEquals(false, true);
    }
    //@Test
    public void testAddEntry(){
        Log.i("Npt", "Ddf");
        // Here i have my new database wich is not connected to the standard database of the App
    }


    @Override
    public void successstore(ArrayList<ChatMessage> chatMessages) {



        SQLiteDatabase db = mMyopenhelper.getReadableDatabase();

        Cursor res = db.rawQuery("select * from " + DICTIONARY_TABLE_NAME, null);

        if(res.getCount()==0)
        {
            System.out.println(res);
        }

    }

    @Override
    public void failurestore() {

    }

    @Override
    public void progressstarted() {

    }

    @Override
    public void progresscompleted() {

    }
}
