package com.example.welcome.chatprojectinmvpmodel;

import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model.registermodel;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model.signinmodel;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.registerview;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.signinview;
import com.example.welcome.chatprojectinmvpmodel.Models.ChatMessage;
import com.example.welcome.chatprojectinmvpmodel.Others.Constant;
import com.example.welcome.chatprojectinmvpmodel.Others.Constants;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static com.example.welcome.chatprojectinmvpmodel.Others.Constants.VALIDNAMEERROR;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNotSame;
import static junit.framework.Assert.assertNull;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class Registeractivitytest implements registerview {

    private registermodel mregistermodel;
    private ArrayList<ChatMessage> chatMessages;



    @Before
    public void setUp() throws Exception {


        mregistermodel = new registermodel(this);
        mregistermodel.customerdetailsforregistering("venkatesh","venkateshsodt@gmai.com","12356");
        chatMessages = new ArrayList<>();

    }

    @After
    public void tearDown() throws Exception {



    }

    @Test
    public void testlaunch() {

    }



    @Override
    public void registerofflinevalidationsucess(String username, String useremail, String password) {




    }

    @Override
    public void registersuccess() {

    }

    @Override
    public void registerationfailure(String message) {

        assertEquals(message,Constants.VALIDNAMEERROR);
        System.out.println(message);
    }

    @Override
    public void networkfailure(String message) {

    }

    @Override
    public void showprogressview() {

    }

    @Override
    public void hideprogressview() {

    }
}