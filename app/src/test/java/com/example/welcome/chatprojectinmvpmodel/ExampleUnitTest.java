package com.example.welcome.chatprojectinmvpmodel;

import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model.chatmodel;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.chatview;
import com.example.welcome.chatprojectinmvpmodel.Models.ChatMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest implements chatview {

    private chatmodel mchatmodel;
    private ArrayList<ChatMessage> chatMessages;



    @Before
    public void setUp() throws Exception {


        mchatmodel = new chatmodel(ExampleUnitTest.this);
       // mchatmodel.presentstring("Venkat");
        chatMessages = new ArrayList<>();

    }

    @After
    public void tearDown() throws Exception {



    }

    @Test
    public void testlaunch() {

    }


    @Override
    public void sendmessage(ChatMessage chatMessage) {
        assertEquals(chatMessage.getMessageText(),"Venkat");

    }

    @Override
    public void successstore(ArrayList<ChatMessage> chatMessage) {

    }

    @Override
    public void end(String text) {

    }

    @Override
    public void retry(String text) {

    }

    @Override
    public void progressdisplayon() {

    }

    @Override
    public void progressdisplayoff() {

    }
}