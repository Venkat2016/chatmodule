package com.example.welcome.chatprojectinmvpmodel;

import android.util.Log;

import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model.chatmodel;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model.signinmodel;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.chatview;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.signinview;
import com.example.welcome.chatprojectinmvpmodel.Models.ChatMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class Chattest implements chatview {

    private chatmodel mchatmodel;
    @Mock
    private ArrayList<ChatMessage> chatMessages;



    @Before
    public void setUp() throws Exception {

        chatMessages = new ArrayList<>();

        mchatmodel = new chatmodel(this);
      //  mchatmodel.presentstring("hjkkk");


    }

    @After
    public void tearDown() throws Exception {



    }

    @Test
    public void testlaunch() {

        successstore(chatMessages);

    }



    @Override
    public void sendmessage(ChatMessage chatMessage) {

       // assertEquals(chatMessage.getMessageText(),"9840649860");


        chatMessages.add(chatMessage);
        assertNotNull(chatMessage.getMessageText());
      //  assertEquals(chatMessage.getMessageText(),"io");

        //System.out.println(chatMessage.getMessageText());

    }

    @Override
    public void successstore(ArrayList<ChatMessage> chatMessage) {

        System.out.print(chatMessage.size());

        System.out.println(chatMessage.get(0).getUserType());
      //  assertEquals(chatMessage.get(1).getMessageText(),"49860");
    }

    @Override
    public void end(String text) {

    }

    @Override
    public void retry(String text) {

    }

    @Override
    public void progressdisplayon() {

    }

    @Override
    public void progressdisplayoff() {

    }
}