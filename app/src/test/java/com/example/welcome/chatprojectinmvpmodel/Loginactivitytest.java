package com.example.welcome.chatprojectinmvpmodel;

import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model.chatmodel;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model.signinmodel;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.chatview;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.signinview;
import com.example.welcome.chatprojectinmvpmodel.Models.ChatMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class Loginactivitytest implements signinview {

    private signinmodel msigninmodel;
    private ArrayList<ChatMessage> chatMessages;



    @Before
    public void setUp() throws Exception {


        msigninmodel = new signinmodel(this);
        msigninmodel.customercredentials("9840649860","1234567");
        chatMessages = new ArrayList<>();

    }

    @After
    public void tearDown() throws Exception {



    }

    @Test
    public void testlaunch() {

    }



    @Override
    public void signinofflinevalidationsucess(String username, String password) {

       assertEquals(username,"9840649860");
        assertEquals(password,"1234567");

    }

    @Override
    public void signinsuccess() {

    }

    @Override
    public void signinfailure(int message) {

        assertEquals(message,0x7f070017);
       //for empty password
       // assertEquals(message,0x7f07001a);
        // not reached lenght
        //assertEquals(message,0x7f070019);

    }

    @Override
    public void networkfailure(String message) {

    }

    @Override
    public void showprogressview() {

    }

    @Override
    public void hideprogressview() {

    }
}