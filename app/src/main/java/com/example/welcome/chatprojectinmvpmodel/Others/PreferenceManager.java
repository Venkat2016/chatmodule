package com.example.welcome.chatprojectinmvpmodel.Others;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Venkatesh on 27-09-2017.
 */

public class PreferenceManager {


    private static final String PREF_NAME = "CHATAPP";
    // Only this variable going to save as permanent
    private String USERNAME;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;


    public PreferenceManager(Context context) {

        this._context = context;
        int PRIVATE_MODE = 0;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();

    }

    public String getUsername() {
        if(pref.getString(USERNAME, "").isEmpty() || pref.getString(USERNAME, "").equals(""))
        {
            return "";
        }
        else
        {
            return pref.getString(USERNAME, "");
        }


    }

    public void setUsername(String username) {

        editor.putString(USERNAME, username);
        editor.apply();

    }
}
