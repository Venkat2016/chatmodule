package com.example.welcome.chatprojectinmvpmodel.Activity;

import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Venkatesh on 14-11-2017.
 */

@RequiresApi(api = Build.VERSION_CODES.M)
public class AuthenticationHandler extends FingerprintManager.AuthenticationCallback{

    private Fingerprintauthenticatonactivity fingerprintactivity;

    public AuthenticationHandler(Fingerprintauthenticatonactivity fingerprintactivity) {

        this.fingerprintactivity = fingerprintactivity;
    }

    public AuthenticationHandler() {
        super();
    }

    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
        super.onAuthenticationError(errorCode, errString);
       // Toast.makeText(fingerprintactivity, "", Toast.LENGTH_SHORT).show();

        Log.i("Checkingfingerprint", "Auth Error: " + errString);
    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        super.onAuthenticationHelp(helpCode, helpString);
        Log.i("Checkingfingerprint", "Auth Failed: ");
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        super.onAuthenticationSucceeded(result);
        Toast.makeText(fingerprintactivity, "Authentication success", Toast.LENGTH_SHORT).show();
        Log.i("Checkingfingerprint", "Auth Success: ");
    }

    @Override
    public void onAuthenticationFailed() {

        super.onAuthenticationFailed();
    }
}
