package com.example.welcome.chatprojectinmvpmodel.Retrofitclasses;

/**
 * Created by VENKATESH on 2/21/2017.
 */


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Pathwithgetstaticclass {

    private static Retrofit retrofit = null;

    private static Retrofit retro = null;
    /*
       private static final String APPMAINURL = "http://139.59.45.116:8082/";
    */
 // private static final String APPMAINURL = "http://93.115.28.38:8082/";
   //private static final String APPMAINURL = "http:192.168.0.173:8082/";

    private static final String APPMONGOURL = "http://192.168.0.187:3000/";

    // private static final String APPMAINURL = "http:192.168.0.124:8082/";
  // private static final String APPMAINURL = "http://192.168.0.173:8082/";//Appgram
   private static final String APPMAINURL = "http://192.168.1.12:8082/";//Ebisu
/*
private static final String APPMAINURL = "http://192.168.0.197:8082/";
*/

    public static Retrofit getClient() {

/*
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(APPMAINURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build();
        }
        return retrofit;
*/


        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        okhttp3.Response response = chain.proceed(request);

                        // todo deal with the issues the way you need to
                        if (response.code() == 500) {

                            return response;
                        }

                        return response;
                    }
                })
                .build();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(APPMAINURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;

    }

/*
    Retrofit retrofit = builder.build();
    As you can see in the snippet above, the okhttp3.Response response = chain.proceed(request); line accesses the server res
*/
    public static Retrofit getmongoClient(String url) {


        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();

        if (retro == null) {
            retro = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build();
        }
        return retrofit;


    }


    public static Retrofit getauthorizedclient(OkHttpClient httpClient) {

        Retrofit retofit = new Retrofit.Builder()
                .baseUrl("http://192.168.0.173:8082/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();

        return retofit;

    }

    public static Retrofit getputclient(OkHttpClient httpClient) {
        Retrofit retofit = new Retrofit.Builder()
                .baseUrl("http://192.168.0.163:8082/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();

        return retofit;


    }


}
