package com.example.welcome.chatprojectinmvpmodel.Others;

/**
 * Created by madhur on 3/1/15.
 */
public class Constants {

/*
    public static final String TAG="chatbubbles";
    public static final String VALIDMOBILENUMBERERORR = "Please Enter Valid Mobile";
*/
    public static final String VALIDNAMEERROR = "Please Enter Valid Name";
    public static final String VALIDPASSWORDERROR = "Please Enter Password";
    public static final String PASSWORDWITHLENGTHERROR = "Please Enter Password with 6 characters least";
    public static final String VALIDEMAILERROR = "Please Enter Valid Email";

}
