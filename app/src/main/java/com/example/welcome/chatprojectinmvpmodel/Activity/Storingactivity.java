package com.example.welcome.chatprojectinmvpmodel.Activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model.storagemodel;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.storageview;
import com.example.welcome.chatprojectinmvpmodel.Models.ChatMessage;
import com.example.welcome.chatprojectinmvpmodel.R;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class Storingactivity extends AppCompatActivity implements storageview {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storingactivity);

       // saveTextFile(this,"android.txt","This is your first file saved");

        storagemodel mstoragemodel = new storagemodel(this);

        mstoragemodel.getvaluesfromdb(Storingactivity.this);


    }



    public void saveTextFile(Context context, String sFileName, String sBody) {
        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        File filePath = new File(root + "/Personal Encyclopedia/Data" + File.separator);
        if (!filePath.exists()) {
            filePath.mkdirs();
        }
        File writeFile = new File(filePath, sFileName);
        FileWriter writer = null;
        try {
            writer = new FileWriter(writeFile, true);

            for(int i=0;i<7;i++) {
                writer.append(sBody);
            }
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void successstore(ArrayList<ChatMessage> chatMessages) {

        Toast.makeText(this,chatMessages.size()+"", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void failurestore() {

        Toast.makeText(this, "Not Successfuly inserted", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void progressstarted() {

    }

    @Override
    public void progresscompleted() {

    }
}
