package com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.util.Log;

import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Presenter.chatpresenter;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.chatview;
import com.example.welcome.chatprojectinmvpmodel.Models.ChatMessage;
import com.example.welcome.chatprojectinmvpmodel.Models.UserType;
import com.example.welcome.chatprojectinmvpmodel.Others.CheckNetwork;
import com.example.welcome.chatprojectinmvpmodel.Others.Constant;
import com.example.welcome.chatprojectinmvpmodel.Retrofitclasses.Pathwithgetstaticclass;
import com.example.welcome.chatprojectinmvpmodel.Retrofitinterface.APIInterface;
import com.example.welcome.chatprojectinmvpmodel.sqlitefiles.Myopenhelper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.welcome.chatprojectinmvpmodel.sqlitefiles.Myopenhelper.DICTIONARY_TABLE_NAME;

/**
 * Created by welcome on 14-09-2017.
 */

public class chatmodel implements chatpresenter {

    private chatview mchatview;
    private APIInterface service;
    public static int session;
    private Call<ResponseBody> mCall;
    private ChatMessage messagesend;

    private boolean isfirst = true;
    private String date;
    private Context context;
    private static final String KEY_WORD = "Admin";
    private static final String KEY_DEFINITION = "Chatuser";
    private static final String KEY_DATE = "DATE";
    private static final String KEY_TIME = "TIME";
    private static final String KEY_STATUS = "STATUS";
    private String passed;
    private String errorcode = "";
    private String finals = "";
    private String modestatus = "";
    private String mode = "";
    public static String sessionkey;
    private String error_text = "";
    private Constant constant;
    private JSONArray texts = null;

    private Boolean optionspresent = false;
    private String optionsIsThere;
    private boolean calltransfer = false;

    public chatmodel(chatview chatview) {
        this.mchatview = chatview;
    }


    //Sir demo client_id
    @Override
    public boolean presentstring(String passedvariable, Context context) {


        this.context = context;
        this.passed = passedvariable;
        constant = new Constant();


        date = new SimpleDateFormat("dd/MM/yyyy").format(new Date());


        messagesend = new ChatMessage();
        messagesend.setMessageStatus("SENT");
        messagesend.setMessageText(passedvariable); // 10 spaces;
        messagesend.setUserType(UserType.OTHER);

        updateoperation();
        messagesend.setMessagedate(date);
        messagesend.setMessageTime(new Date().getTime());


        if (CheckNetwork.isInternetAvailable(context)) {
            mchatview.progressdisplayon();
            if (passedvariable.equals("PIAI")) {

            } else {

                mchatview.sendmessage(messagesend);
            }

            if (isfirst) {
                Random r = new Random();
                session = (r.nextInt(9999999) + 1000000);

                sessionkey = constant.CliendId + "and" + session;
                networkoperations(messagesend, sessionkey);
            } else {

                networkoperations(messagesend, sessionkey);
            }
        } else {
            mchatview.retry("OOPS No Network Connection");
        }

        return false;
    }

    private void networkoperations(final ChatMessage messages, final String session) {


        //  Log.i("Checkurl", "http://" + URL + ":8082/");
        service = Pathwithgetstaticclass.getClient().create(APIInterface.class);


        JsonObject object = new JsonObject();
        JsonObject obj2 = new JsonObject();
        JsonArray arr1 = new JsonArray();

        JsonObject obj3 = new JsonObject();

/*
        object.addProperty("client_id", "efree3067");
        object.addProperty("client_key", "MazOBHmMJJu4FdkIztQBiPaMJKPpObVryf6JqoAaLKRVO0YxH7hp8ZV8BwRzp0uw");
*/
        object.addProperty("client_id", constant.CliendId);
        object.addProperty("client_key", constant.ClientKey);

        //  object.addProperty("client_id", "eacce7959");
        /*  object.addProperty("client_key", "3XdjU11DY5Ct6PSKT8B0fUzYWpapBJDPnkRnJ6uHEfdvrNtual7llFyodDtARom5" +
                  "");*/

        object.addProperty("domain", "restaurant");
        object.addProperty("customer_id", "00456578");

        object.addProperty("session", sessionkey);


        if (isfirst || mode.equals("queue")) {

            Log.i("Chekcing", object.toString());
            Log.i("Check", ":ch");

        } else {
            object.add("result", obj2);
            obj2.addProperty("resulttype", "Partial");

            obj2.add("alts", arr1);


            obj3.addProperty("confidence", "1");


            obj3.addProperty("transcript", messages.getMessageText());

            arr1.add(obj3);
            Log.i("Chekcing", object.toString());
        }


        if (mode.equals("queue")) {
            mCall = service.getpythonqueue(object);

        } else {
            if (isfirst) {
                mCall = service.getpython(object);
                isfirst = false;
            } else {
                mCall = service.getpythonnext(object);
            }
        }


        mCall.enqueue(new Callback<ResponseBody>() {


            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                // this method will execute when reaches the status code 200
                if (response.code() == 200) {

                    // mProgressDialog.dismiss();

/*
                    try {
                        Log.i("Check", response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
*/
/*
                    try {
                        Log.i("Cgekj", response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
*/

/*
                    try {
                        Log.i("Checkresp", response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
*/

                    mchatview.progressdisplayoff();
                    JSONObject js;

                    try {

                        // mchatview.retry(response.body().string());
                        js = new JSONObject(response.body().string());


                        Log.i("Checkingresponse", js + "");

                       // Agentstatus = true;
                        constant.setAgentstatus(true);
                        error_text = js.optString("error_message");
                        finals = js.optString("final");
                        if (error_text.length() > 0) {
                            mchatview.retry(error_text);
                            mchatview.end(error_text);
                        } else {
                            String messagetext = js.optString("text");
                            mode = js.optString("mode");




/*
                            mchatview.retry(texts.get(0)+"");
                            mchatview.retry(texts.get(1)+"");
                            mchatview.retry(texts.get(2)+"");
*/
                            String queue = js.optString("queue");
                            errorcode = js.optString("error_code");
                            calltransfer = js.optBoolean("call_transfer_flag");

                            optionsIsThere = js.optString("optionsIsThere");


                            if (optionsIsThere.equals("true")) {
                                optionspresent = true;
                                texts = js.optJSONArray("options");

                            } else {
                                optionspresent = false;
                            }

                            if (finals.equals("true")) {

                                if (calltransfer) {

                                    mchatview.retry("Call has forwarded to manual");
                                    mchatview.manualtransfer();


                                } else {

                                    mchatview.retry("Good Bye");
                                    mchatview.end("finish");
                                }
                            }


                            if (mode.equals("queue") || (queue.equals("true"))) {

                                mchatview.retry(messagetext);
                                new Handler().postDelayed(new Runnable() {


                                    @Override
                                    public void run() {

                                        networkoperations(messages, sessionkey);


                                    }
                                }, 10000);


                            } else if (mode.equals("next")) {


                                // if (errorcode.equals("") || errorcode.equals(null) || errorcode.length() == 0) {

                                //      if (finals.equals("") || finals.equals(null) || finals.length() == 0) {


                                if (finals.equals("true")) {
                                    if (calltransfer) {
                                        constant.setAgentstatus(false);
                                        //Agentstatus = false;
                                        mchatview.retry("Call has forwarded to manual");

                                        mchatview.manualtransfer();
                                        response.body().close();

                                    } else {
                                        mchatview.retry("Good Bye");
                                    }
                                } else {
                                    if (optionspresent) {

                                        //  mchatview.retry(texts.length()+"");
                                        optionspresent = false;
                                        final ChatMessage message = new ChatMessage();


                                        message.setOptions(texts);
                                        message.setMessageText(messagetext);
                                        message.setUserType(UserType.MULTIOPTIONS);
                                        message.setMessageTime(new Date().getTime());

                                        updateoperation();

                                        mchatview.sendmessage(message);


                                    } else if (!optionspresent) {

                                        final ChatMessage messagsse = new ChatMessage();


                                        messagsse.setMessageText(messagetext); // 10 spaces;
                                        messagsse.setUserType(UserType.COOL);
                                        messagsse.setMessageTime(new Date().getTime());


                                        updateoperation();
                                        mchatview.sendmessage(messagsse);
                                    }
                                }

                                //Log.i("Schd", errorcode);


                                //  mProgressDialog.dismiss();


                            } else {

                                if (finals.equals("true")) {

                                    if (calltransfer) {
                                        mchatview.retry("Call has forwarded to manual");
                                        mchatview.manualtransfer();
                                        response.body().close();

                                    } else {
                                        mchatview.end("finish");
                                    }
                                } else {

                                    if (optionspresent) {

                                        optionspresent = false;
                                        //  mchatview.retry(texts.length()+"");
                                        final ChatMessage message = new ChatMessage();


                                        message.setOptions(texts);
                                        message.setMessageText(messagetext);
                                        message.setUserType(UserType.MULTIOPTIONS);
                                        message.setMessageTime(new Date().getTime());
                                        updateoperation();
                                        mchatview.sendmessage(message);


                                    } else if (!optionspresent) {


                                        final ChatMessage messagess = new ChatMessage();


                                        messagess.setMessageText(messagetext); // 10 spaces;
                                        messagess.setUserType(UserType.COOL);

                                        messagess.setMessageTime(new Date().getTime());


                                        updateoperation();
                                        mchatview.sendmessage(messagess);
                                    }
                                }

                            }
                        }
                    } catch (Exception e) {


                        Log.i("Ech", e.toString());
                    }


                }else if(response.code()==500)
                {
                    mchatview.retry("500");
                }
                else {


                    mchatview.progressdisplayoff();

                  //  Agentstatus = false;
                    constant.setAgentstatus(false);
                    mchatview.retry(response.code()+"");
                    try {
                        mchatview.retry(response.errorBody().string());
                        Log.i("Checkingrespnon", response.errorBody().string());


                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //  constant.alertdialogwithcallresponse(Splashscreen.this, getResources().getString(R.string.Experiencetechnical), call);

                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //  mProgressDialog.dismiss();
               // Agentstatus = false;
                constant.setAgentstatus(false);
                mchatview.progressdisplayoff();
                if (t instanceof ConnectException) {
                    Log.i("Checksocket", "Connect");
                    mchatview.retry("Connection Exception");
                }

                if (t instanceof SocketTimeoutException) {
                    Log.i("Checksocket", "Socket");
                    mchatview.retry("Socket Exception");
                }


            }

        });

    }


    private void updateoperation() {
        Myopenhelper myopenhelper = new Myopenhelper(context);

        SQLiteDatabase db = myopenhelper.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(KEY_STATUS, "Delivered");

        String selection = KEY_DEFINITION + " =?";
        String[] selectionArgs = {passed};


        int count = db.update(
                DICTIONARY_TABLE_NAME,
                cv,
                selection,
                selectionArgs);

        if (count == -1) {
            Log.i("Statis", "Failed");
        } else {
            Log.i("Statis", "Passed");
        }
/*
        db.rawQuery("UPDATE "+ DICTIONARY_TABLE_NAME +
                        " SET " + KEY_STATUS + " = " + "Delivered" +
                        " WHERE " + KEY_DEFINITION + " = ?",
                new String[] { passed });
*/

        //  db.execSQL("UPDATE DB_TABLE SET "+KEY_STATUS+"="+" READ"+" WHERE " + KEY_DEFINITION + passed);
        //  db.update(DICTIONARY_TABLE_NAME, cv, KEY_DEFINITION + messagetext, null);

        //  db.update(DICTIONARY_TABLE_NAME, cv, KEY_DEFINITION + "=" + passed, null);

    }


}
