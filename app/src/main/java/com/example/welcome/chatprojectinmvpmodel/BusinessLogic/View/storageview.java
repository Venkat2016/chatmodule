package com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View;

import com.example.welcome.chatprojectinmvpmodel.Models.ChatMessage;

import java.util.ArrayList;


/**
 * Created by welcome on 23-09-2017.
 */

public interface storageview {


    void successstore(ArrayList<ChatMessage> chatMessages);


    void failurestore();

    void progressstarted();

    void progresscompleted();

}
