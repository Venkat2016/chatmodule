package com.example.welcome.chatprojectinmvpmodel.Activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.welcome.chatprojectinmvpmodel.Abstraction.AbsRuntimePermission;
import com.example.welcome.chatprojectinmvpmodel.R;


public class HomeActivity extends AbsRuntimePermission {

    private static final int REQUEST_PERMISSION = 101;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.multipleitems);

        requestAppPermissions(new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO},
                R.string.msg, REQUEST_PERMISSION);


    }

    @Override
    public void onPermissionsGranted(int requestCode) {
        Intent i = new Intent(HomeActivity.this, ChatActivity.class);
        startActivity(i);
    }

    @Override
    protected void onPermissionnotgranted() {

    }

}
