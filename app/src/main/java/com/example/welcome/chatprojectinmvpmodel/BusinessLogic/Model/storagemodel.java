package com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Presenter.storagepresenter;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.storageview;
import com.example.welcome.chatprojectinmvpmodel.Models.ChatMessage;
import com.example.welcome.chatprojectinmvpmodel.Models.UserType;
import com.example.welcome.chatprojectinmvpmodel.Others.Backgroundthread;
import com.example.welcome.chatprojectinmvpmodel.sqlitefiles.Myopenhelper;

import java.util.ArrayList;

import static com.example.welcome.chatprojectinmvpmodel.sqlitefiles.Myopenhelper.DICTIONARY_TABLE_NAME;

/**
 * Created by Venkatesh on 23-09-2017.
 */

public class storagemodel implements storagepresenter {

    private storageview mstorageview;
    private ArrayList<ChatMessage> chatMessages;
    private ChatMessage chatMessage;




    public storagemodel(storageview mstorageview) {

        mstorageview.progressstarted();
        this.mstorageview = mstorageview;

    }

    @Override
    public void getvaluesfromdb(Context context) {


/*
        ArrayList<String> demo = new ArrayList<>();
        demo.add("Hello");
        demo.add("Cool");
        demo.add("its");
        demo.add("just");
        demo.add("ademo");
*/


      //  Toast.makeText(context,new Backgroundthread(context,demo).toString(), Toast.LENGTH_SHORT).show();

       // new Backgroundthread(context, demo).execute();

       /* Myopenhelper myopenhelper = new Myopenhelper(context);
        SQLiteDatabase db = myopenhelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_WORD,username);
        contentValues.put(KEY_DEFINITION,password);
        long result = db.insert(DICTIONARY_TABLE_NAME, null, contentValues);
        if(result==-1)
        {
            mstorageview.failurestore();
        }
        else
        {
            mstorageview.successstore();
b
        }
*/



        chatMessages = new ArrayList<>();
        Cursor res = getdata(context);
        if(res.getCount()==0)
        {
            mstorageview.failurestore();
        }
        else
        {
           // StringBuffer stringbuffer = new StringBuffer();
            while (res.moveToNext())
            {
/*
                stringbuffer.append("admin:" + res.getString(0));
                stringbuffer.append("chatuser" + res.getString(1));
                stringbuffer.append("time" + res.getString(3));
*/

                if(res.getString(0)==null) {
                    chatMessage = new ChatMessage();
                    chatMessage.setUserType(UserType.OTHER);
                    chatMessage.setMessageText(res.getString(1));
                    chatMessage.setMessagedate(res.getString(2));
                    chatMessage.setMessageStatus(res.getString(3));

                    chatMessage.setMessageTime(Long.parseLong(res.getString(4)));
                }
                else
                {
                    chatMessage = new ChatMessage();
                    chatMessage.setUserType(UserType.COOL);
                    chatMessage.setMessageText(res.getString(0));
                    chatMessage.setMessagedate(res.getString(2));
                    chatMessage.setMessageStatus(res.getString(3));
                    Log.i("Check", res.getString(3));
                    chatMessage.setMessageTime(Long.parseLong(res.getString(4)));

                }

                chatMessages.add(chatMessage);


                Log.i("Checkingcound", res.getCount() + "");

            }



            res.close();
            mstorageview.progresscompleted();
            mstorageview.successstore(chatMessages);

        }

    }

   private Cursor getdata(Context context) {

              Myopenhelper myopenhelper = new Myopenhelper(context);

       SQLiteDatabase db = myopenhelper.getReadableDatabase();

       return db.rawQuery("select * from " + DICTIONARY_TABLE_NAME, null);

    }


    public void sendvaluesupdated(Context context, ArrayList<ChatMessage> chatmessage) {


        mstorageview.progresscompleted();
        new Backgroundthread(context, chatmessage).execute();


    }

}
