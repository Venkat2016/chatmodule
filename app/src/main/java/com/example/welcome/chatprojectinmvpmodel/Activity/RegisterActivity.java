package com.example.welcome.chatprojectinmvpmodel.Activity;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;

import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model.registermodel;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.registerview;
import com.example.welcome.chatprojectinmvpmodel.R;


public class RegisterActivity extends AppCompatActivity implements registerview {

    private ProgressBar progressBar;

    private registermodel mregistermodel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        progressBar = (ProgressBar) findViewById(R.id.determinateBar);

        mregistermodel = new registermodel(RegisterActivity.this);

        mregistermodel.customerdetailsforregistering("9840649860","venkateshsoft14@gmail.com","123456789");

    }

    @Override
    public void registerofflinevalidationsucess(String username, String useremail, String password) {

        snackbarmethod(username);

    }

    private void snackbarmethod(String message) {
        Snackbar.make(findViewById(android.R.id.content), message,
                Snackbar.LENGTH_SHORT).show();

    }

    @Override
    public void registersuccess() {

    }

    @Override
    public void registerationfailure(String message) {

    }

    @Override
    public void networkfailure(String message) {

    }

    @Override
    public void showprogressview() {

    }

    @Override
    public void hideprogressview() {

    }
}
