package com.example.welcome.chatprojectinmvpmodel.Adapter;


import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.welcome.chatprojectinmvpmodel.Activity.ChatActivity;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.itemclick;
import com.example.welcome.chatprojectinmvpmodel.EventBusClasses.Customloginresponsemessage;
import com.example.welcome.chatprojectinmvpmodel.Models.ChatMessage;
import com.example.welcome.chatprojectinmvpmodel.Models.Status;
import com.example.welcome.chatprojectinmvpmodel.Models.UserType;
import com.example.welcome.chatprojectinmvpmodel.R;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.OnItemClick;

public class ChatListAdapter extends BaseAdapter {

    private List<ChatMessage> chatMessages;
    private Context context;
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("h:mm a", Locale.US);
    private ClipboardManager myClipboard;
    private ClipData myClip;
    private itemclick mCallback;
    private Boolean matched = false;

    public ChatListAdapter(List<ChatMessage> chatMessages, Context context, itemclick listener) {
        this.chatMessages = chatMessages;
        this.context = context;
        this.mCallback = listener;

    }

    public ChatListAdapter(List<ChatMessage> chatMessages, Context context) {
        this.chatMessages = chatMessages;
        this.context = context;
    }


    @Override
    public int getCount() {
        return chatMessages.size();
    }

    @Override
    public Object getItem(int position) {

        return chatMessages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = null;
        final ChatMessage message = chatMessages.get(position);
        final ViewHolder1 holder1;
        final ViewHolder2 holder2;
        //ViewHolder3 holder3;
        final ViewHolder4 holder4;

        if (message.getUserType() == UserType.COOL) {
            if (convertView == null) {
                //  v = LayoutInflater.from(context).inflate(R.layout.chat_user1_item, null, false);
                v = LayoutInflater.from(context).inflate(R.layout.chat_user1_item, parent, false);
                holder1 = new ViewHolder1();


                holder1.messageTextView = (TextView) v.findViewById(R.id.textview_message);
                holder1.timeTextView = (TextView) v.findViewById(R.id.textview_time);
                // holder1.textview_date = (TextView) v.findViewById(R.id.textview_date);

                v.setTag(holder1);
            } else {
                v = convertView;
                holder1 = (ViewHolder1) v.getTag();

            }

            if (holder1 == null) {

            } else {

                Log.i("Checcking", message.getMessageText());
                holder1.messageTextView.setText(Html.fromHtml(message.getMessageText()
                        + " &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;"));
                //   holder1.timeTextView.setText(SIMPLE_DATE_FORMAT.format(message.getMessageTime()));
//               holder1.textview_date.setText(message.getMessagedate());

            }

        } else if (message.getUserType() == UserType.OTHER) {

            if (convertView == null) {
                v = LayoutInflater.from(context).inflate(R.layout.chat_useritem2, parent, false);

                holder2 = new ViewHolder2();


                holder2.messageTextView = (TextView) v.findViewById(R.id.textview_message);
                holder2.timeTextView = (TextView) v.findViewById(R.id.textview_time);
                holder2.messageStatus = (ImageView) v.findViewById(R.id.user_reply_status);
                // holder2.textview_date = (TextView) v.findViewById(R.id.textview_date);

                v.setTag(holder2);

            } else {
                v = convertView;
                holder2 = (ViewHolder2) v.getTag();

            }

            if (holder2 == null) {

            } else {
                holder2.messageTextView.setText(Html.fromHtml(message.getMessageText()
                        + " &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160&#160;&#160;&#160;&#160; "));
                holder2.timeTextView.setText(SIMPLE_DATE_FORMAT.format(message.getMessageTime()));
                //  holder2.textview_date.setText(message.getMessagedate());
                holder2.timeTextView.setText(SIMPLE_DATE_FORMAT.format(message.getMessageTime()));

//                Log.i("Checkingstatus", message.getMessageStatus());


                final String text = holder2.messageTextView.getText().toString();
                holder2.messageTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        myClipboard = (ClipboardManager) v.getContext().getSystemService(Context.CLIPBOARD_SERVICE);


                        String trimmed = text.replace(String.valueOf((char) 160), " ").trim();
                        myClip = ClipData.newPlainText("label", trimmed);
                        myClipboard.setPrimaryClip(myClip);
                        //Toast.makeText(context,trimmed, Toast.LENGTH_SHORT).show();


                    }
                });
/*                if (message.getMessageStatus().equals("NotDelivered")) {
                    holder2.messageStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.message_got_receipt_from_target));
                } else if (message.getMessageStatus().equals("SENT")) {

                    holder2.messageStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.message_got_receipt_from_server));

                } else {
                    if (message.getMessageStatus().equals("Delivered")) {


                        holder2.messageStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.message_got_read_receipt_from_target_onmedia));

                    } else {
                        if (message.getMessageStatus().equals("READ")) {

                            holder2.messageStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.message_got_read_receipt_from_target_onmedia));

                        }
                    }
                }*/

            }


        }
        if (message.getUserType() == UserType.MULTIOPTIONS) {
            if (convertView == null) {
                //  v = LayoutInflater.from(context).inflate(R.layout.chat_user1_item, null, false);
                v = LayoutInflater.from(context).inflate(R.layout.multipleitems, null, false);
                holder4 = new ViewHolder4();

                holder4.title = (TextView) v.findViewById(R.id.messagetitle);
                holder4.option1 = (TextView) v.findViewById(R.id.option1);
                holder4.option2 = (TextView) v.findViewById(R.id.option2);
                holder4.option3 = (TextView) v.findViewById(R.id.option3);

                v.setTag(holder4);
            } else {
                v = convertView;
                holder4 = (ViewHolder4) v.getTag();

            }

            if (holder4 == null) {

            } else {
/*
                holder4.option1.setText(Html.fromHtml(message.getMessageText()
                        + " &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;"));
                holder4.timeTextView.setText(SIMPLE_DATE_FORMAT.format(message.getMessageTime()));
//                holder1.textview_date.setText(message.getMessagedate());
                final ChatActivity chatActivity = null;
                holder4.timeTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Customloginresponsemessage custommessageevent = new Customloginresponsemessage();
                        custommessageevent.setUseremail(holder4.messageTextView.getText().toString());
                        EventBus.getDefault().post(custommessageevent);

                        Intent i = new Intent(context, ChatActivity.class);
                        context.startActivity(i);
                    }
                });
*/

                try {

                    holder4.title.setText(message.getMessageText());

                    holder4.option1.setText(message.getOptions().get(0) + "");
                    holder4.option2.setText(message.getOptions().get(1) + "");
                    holder4.option3.setText(message.getOptions().get(2) + "");


                    Handler handler = new Handler();
                    Runnable r1 = new Runnable() {
                        public void run() {
                            try {

                                if (holder4.title.getText().toString().equals(chatMessages.get(position).getMessageText())) {

                                    matched = true;
                                } else {
                                    matched = false;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    };
                    handler.postDelayed(r1, 500);

                    holder4.option1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            try {

                                //  Log.i("Checking text compare", holder4.option1.getText().toString() + "Compare to" + chatMessages.get(position).getOptions().get(0));

                                if (matched) {

                                    matched = false;
    /*
                                Customloginresponsemessage custommessageevent = new Customloginresponsemessage();
                                custommessageevent.setUseremail(holder4.option1.getText().toString());
                                EventBus.getDefault().post(custommessageevent);
    */

                                    try {
                                        mCallback.onClick(holder4.option1.getText().toString(),position);
                                        holder4.option1.setVisibility(View.INVISIBLE);
                                        holder4.option2.setVisibility(View.INVISIBLE);
                                        holder4.option3.setVisibility(View.INVISIBLE);
                                        holder4.option1.setEnabled(false);
                                        holder4.option2.setVisibility(View.INVISIBLE);
                                        holder4.option3.setVisibility(View.INVISIBLE);
                                    } catch (Exception e) {
                                        Toast.makeText(context, "your request not accepted", Toast.LENGTH_SHORT).show();
                                    }


                                /*if (countopt1 == 1) {
                                    mCallback.onClick(holder4.option1.getText().toString());
                                    holder4.option1.setVisibility(View.GONE);
                                    holder4.option1.setEnabled(false);

                                } else {

                                }*/

                                } else {
                                    Toast.makeText(context, "your request not accepted not matched", Toast.LENGTH_SHORT).show();

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                    });
                    holder4.option2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            try {
                                if (holder4.option2.getText().toString().equals(chatMessages.get(position).getOptions().get(1)))

                                {
                                    try {
                                        mCallback.onClick(holder4.option2.getText().toString(),position);
                                        holder4.option1.setVisibility(View.INVISIBLE);
                                        holder4.option2.setVisibility(View.INVISIBLE);
                                        holder4.option3.setVisibility(View.INVISIBLE);
                                        holder4.option2.setEnabled(false);
                                    } catch (Exception e) {
                                        Toast.makeText(context, "your request not accepted", Toast.LENGTH_SHORT).show();

                                    }
                                } else {
                                    Toast.makeText(context, "your request not accepted", Toast.LENGTH_SHORT).show();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


/*                            if (countopt2 == 1) {
                                mCallback.onClick(holder4.option2.getText().toString());
                                holder4.option2.setVisibility(View.GONE);
                                holder4.option2.setEnabled(false);
                                countopt2++;

                            } else {
                                holder4.option2.setVisibility(View.GONE);
                                holder4.option2.setEnabled(false);
                            }*/


/*
                            Customloginresponsemessage custommessageevent = new Customloginresponsemessage();
                            custommessageevent.setUseremail(holder4.option2.getText().toString());
                            EventBus.getDefault().post(custommessageevent);
*/

                        }
                    });

                    holder4.option3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
/*
                            Customloginresponsemessage custommessageevent = new Customloginresponsemessage();
                            custommessageevent.setUseremail(holder4.option3.getText().toString());
                            EventBus.getDefault().post(custommessageevent);
*/

                            try {
                                if (holder4.option3.getText().toString().equals(chatMessages.get(position).getOptions().get(2))) {
                                    try {
                                        mCallback.onClick(holder4.option3.getText().toString(),position);
                                        holder4.option1.setVisibility(View.INVISIBLE);
                                        holder4.option2.setVisibility(View.INVISIBLE);
                                        holder4.option3.setVisibility(View.INVISIBLE);
                                        holder4.option3.setEnabled(false);
                                    } catch (Exception e) {
                                        Toast.makeText(context, "your request not accepted", Toast.LENGTH_SHORT).show();


                                    }


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }




/*                            if (count == 1) {
                                mCallback.onClick(holder4.option3.getText().toString());
                                holder4.option3.setVisibility(View.GONE);
                                holder4.option3.setEnabled(false);

                                count++;
                            } else {

                            }*/


                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }/*else
        {
            if (convertView == null) {
                v = LayoutInflater.from(context).inflate(R.layout.chatviewpager, null, false);

                holder3 = new ViewHolder3();
                holder3.recyclerView = (RecyclerView) v.findViewById(R.id.recyclerdisplay);
                v.setTag(holder3);

            } else {
                v = convertView;
                holder3 = (ViewHolder3) v.getTag();

            }

            if(holder3==null)
            {

            }
            else
            {
                holder3.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true));
                holder3.recyclerView.setHasFixedSize(true);
                holder3.recyclerView.setNestedScrollingEnabled(false);
                ChatwithmultipleimageAdapter adapt = new ChatwithmultipleimageAdapter(context, context.getResources().getStringArray(R.array.images));
                holder3.recyclerView.setAdapter(adapt);

            }


        }*/


        return v;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        ChatMessage message = chatMessages.get(position);
        return message.getUserType().ordinal();
    }

    private class ViewHolder1 {
        public TextView messageTextView;
        public TextView timeTextView;
        public TextView textview_date;


    }

    private class ViewHolder2 {
        public ImageView messageStatus;
        public TextView messageTextView;
        public TextView timeTextView;
        public TextView textview_date;

    }
/*
    private class ViewHolder3 {

       public RecyclerView recyclerView;

    }
*/

    private class ViewHolder4 {
        public TextView option1;
        public TextView option2;
        public TextView option3;
        public TextView title;

    }


}
