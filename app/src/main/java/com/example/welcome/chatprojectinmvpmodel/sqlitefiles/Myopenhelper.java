package com.example.welcome.chatprojectinmvpmodel.sqlitefiles;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by welcome on 23-09-2017.
 */

public class Myopenhelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    public static String DICTIONARY_TABLE_NAME = "chat";
    private static final String KEY_WORD = "Admin response" ;
    private static final String KEY_DEFINITION = "Chatuser reply";
    private static final String KEY_DATE = "DATE";
    private static final String KEY_TIME = "TIME";
    private static final String KEY_STATUS = "STATUS";

    private static final String DICTIONARY_TABLE_CREATE =
            "CREATE TABLE " + DICTIONARY_TABLE_NAME + " (" +
                    KEY_WORD + " TEXT, " + KEY_DEFINITION + " TEXT, " + KEY_DATE +" TEXT, " + KEY_STATUS + " TEXT, " +
                    KEY_TIME + " TEXT);";


    public Myopenhelper(Context context) {
        super(context, DICTIONARY_TABLE_CREATE, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DICTIONARY_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+DICTIONARY_TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
