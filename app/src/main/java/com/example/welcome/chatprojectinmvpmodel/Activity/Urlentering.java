package com.example.welcome.chatprojectinmvpmodel.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.welcome.chatprojectinmvpmodel.R;

public class Urlentering extends AppCompatActivity {

    EditText text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_urlentering);
        text = (EditText) findViewById(R.id.url);

    }

    public void submit(View view) {


        Intent i = new Intent(Urlentering.this, ChatActivity.class);
        i.putExtra("URL", text.getText().toString());
        startActivity(i);


    }
}
