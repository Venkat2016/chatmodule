package com.example.welcome.chatprojectinmvpmodel.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.welcome.chatprojectinmvpmodel.Adapter.ChatListAdapter;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model.chatmodel;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model.storagemodel;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model.voicemodel;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.chatview;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.itemclick;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.storageview;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.voiceview;
import com.example.welcome.chatprojectinmvpmodel.EventBusClasses.Customloginresponsemessage;
import com.example.welcome.chatprojectinmvpmodel.Models.ChatMessage;
import com.example.welcome.chatprojectinmvpmodel.Models.UserType;
import com.example.welcome.chatprojectinmvpmodel.Others.CheckNetwork;
import com.example.welcome.chatprojectinmvpmodel.Others.Constant;
import com.example.welcome.chatprojectinmvpmodel.Others.MyApplication;
import com.example.welcome.chatprojectinmvpmodel.Others.NetworkChangeReceiver;
import com.example.welcome.chatprojectinmvpmodel.R;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class ChatActivity extends AppCompatActivity implements itemclick, chatview, voiceview, storageview, TextToSpeech.OnInitListener, NetworkChangeReceiver.ConnectivityReceiverListener, View.OnClickListener {


    //Checking
    private EditText username = null;
    private ListView mListview = null;
    ImageView entervoice = null;
    private chatmodel mchatmodel = null;
    private ArrayList<ChatMessage> chatMessages = null;
    private ArrayList<ChatMessage> specific = null;
    private ChatListAdapter listAdapter = null;
    TextToSpeech t1;
    ImageView ImageButton, searchview;
    private SpeechRecognizer speech = null;
    private Intent recognizerIntent = null;
    private storagemodel mstoragemodel = null;
    ProgressDialog progress = null;
    String Lastspeech;
    ProgressDialog progressDialog;
    private String message;
    // public static String URL;

    // private Boolean Socketconection = false;
    ImageView mute, unmute;
    LinearLayout bottomlayout;

    private Socket mSocket;
    private JSONObject js = null;
    private ChatMessage messagereceived;
    // private Boolean keyboardrequest = false;
    private Constant constant;

    {
        try {
            IO.Options opts = new IO.Options();
            opts.path = "/socket.io-client";
            //  mSocket = IO.socket("http://192.168.0.187:3000", opts);

            mSocket = IO.socket("http://139.59.32.187:3000", opts);
        } catch (URISyntaxException e) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSocket.connect();
        mSocket.on("userAgentMessage:save", onNewMessage);

        //  mSocket.connect();

        // mSocket.connect();
        // mSocket.on("userAgentMessage:save", onNewMessage);

        // Socketconection = true;


/*
        mSocket.connect();
        mSocket.on("userAgentMessage:save", onNewMessage);
*/

        constant = new Constant();
        setContentView(R.layout.activity_main);


//        savedInstanceState.getString("Finalstoppedstring", username.getText().toString());
        progressDialog = new ProgressDialog(ChatActivity.this);
/*
        Intent i = getIntent();
        URL = i.getStringExtra("URL");
*/


        Intializationid(); //Intializing all views

        // updatewitholdchatcontents(); // getting data from db

        settinginput();// input selection

        sendbuttonclicked(); //s buttonclicked

        entervoicebuttonclicked();

        searchbuttonclicked();

        callingapi();

    }


    /* protected void onRestoreInstanceState(Bundle savedInstanceState) {

         if (savedInstanceState.getString("Finalstoppedstring").isEmpty() || savedInstanceState.getString("Finalstoppedstring") == null) {
             username.setText(savedInstanceState.getString("Finalstoppedstring"));
             super.onRestoreInstanceState(savedInstanceState);
         }
     }
 */
    private void callingapi() {
        mchatmodel.presentstring("PIAI", ChatActivity.this);
    }


    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {


            ChatActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    edittextenable();

                    if (!constant.getKeyboardrequest()) {
                        JSONObject data = (JSONObject) args[0];
                        try {
                            //msg_agent_id = data.optString("msg_agent_id");
                            constant.setMsg_agent_id(data.optString("msg_agent_id"));
                            constant.setMsg_flow(data.optString("msg_flow"));
                            // msg_flow = data.optString("msg_flow");
                            // msg_status = data.optString("msg_status");
                            constant.setMsg_status(data.optString("msg_status"));
                            constant.setMsg_session_id(data.optString("msg_session_id"));
                            constant.setMsg_text(data.getString("msg_text"));
                            //String msg_texting = data.getString("msg_text");


                            if (constant.getMsg_session_id().equals(constant.getSessionkey())) {
                                if (constant.getMsg_text() != null || !constant.getMsg_text().isEmpty()) {
                                    final ChatMessage messagetrigger = new ChatMessage();


                                    messagetrigger.setMessageText(constant.getMsg_text()); // 10 spaces;
                                    messagetrigger.setUserType(UserType.COOL);
                                    messagetrigger.setMessageTime(new Date().getTime());
                                    sendmessage(messagetrigger);


                                } else {
                                    Toast.makeText(ChatActivity.this, "It is", Toast.LENGTH_SHORT).show();
                                }


                            }

                        } catch (Exception e) {
                            Log.d("JSON", "friend call object cannot be parsed");
                        }
                    } else {
                        //Toast.makeText(ChatActivity.this, "Keyboard raised", Toast.LENGTH_SHORT).show();
                    }


                }
            });
        }
    };


    public void Intializationid() {


        entervoice = (ImageView) findViewById(R.id.enter_voice);
        username = (EditText) findViewById(R.id.chat_edit_text1);
        mListview = (ListView) findViewById(R.id.chat_list_view);
        ImageButton = (ImageView) findViewById(R.id.enter_chat1);
        mute = (ImageView) findViewById(R.id.muteimage);
        unmute = (ImageView) findViewById(R.id.unmuteimage);
        mchatmodel = new chatmodel(this);
        chatMessages = new ArrayList<>();
        searchview = (ImageView) findViewById(R.id.search);
        progress = new ProgressDialog(this);
        bottomlayout = (LinearLayout) findViewById(R.id.bottomlayout);
        t1 = new TextToSpeech(getApplicationContext(), new voicemodel(this), "com.google.android.tts");
        //EventBus.getDefault().register(this);

        mute.setOnClickListener(this);
        unmute.setOnClickListener(this);
    }

    private void updatewitholdchatcontents() {


        mstoragemodel = new storagemodel(this);

        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {

                mstoragemodel.getvaluesfromdb(ChatActivity.this);


            }
        }, 1000);


    }

    private void entervoicebuttonclicked() {

        entervoice.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED

                        speechinput();


                        return true;
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        Toast.makeText(ChatActivity.this, "Stop listening", Toast.LENGTH_SHORT).show();
                        speech.stopListening();
                        return true;
                }

                return false;
            }
        });

    }

    //This is a supportive fuction for entervoicebuttonclicked method
    private void speechinput() {

        speech = SpeechRecognizer.createSpeechRecognizer(this);
        speech.setRecognitionListener(new voicemodel(this));
        recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE,
                "en");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                this.getPackageName());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);
        speech.startListening(recognizerIntent);

    }


    private void sendbuttonclicked() {
        ImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (constant.getSocketconection()) {


                    //keyboardrequest = true;
                    constant.setKeyboardrequest(true);
                    messagereceived = new ChatMessage();
                    messagereceived.setMessageText(username.getText().toString()); // 10 spaces;
                    messagereceived.setUserType(UserType.OTHER);
                    messagereceived.setMessageTime(new Date().getTime());
                    sendmessage(messagereceived);

                    new MyBackgroundAsyncservice().execute();


                    username.setText("");


                } else {


                    mchatmodel.presentstring(username.getText().toString(), ChatActivity.this);
                    username.setText("");


                }
            }
        });

    }


    @Override
    protected void onResume() {


        super.onResume();


        MyApplication.getInstance().setConnectivityListener(this);

    }


    @Override
    public void onBackPressed() {

        if (constant.getAgentstatus()) {

            mchatmodel.presentstring("bye", ChatActivity.this);


        } else {
            super.onBackPressed();
        }


    }


    @Override
    protected void onStop() {

        Log.i("Dfkdjfj", "Stopstate");
        super.onStop();
    }


    private void settinginput() {
        username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (username.getText().toString().equals("")) {
                    entervoice.setVisibility(View.VISIBLE);
                    ImageButton.setVisibility(View.INVISIBLE);

                } else {

                    entervoice.setVisibility(View.INVISIBLE);
                    ImageButton.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void searchbuttonclicked() {

        searchview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ChatActivity.this, Searchactivity.class);
                startActivity(i);

            }
        });
    }


    @Override
    public void sendmessage(ChatMessage chatMessage) {


        specific = new ArrayList<>();
        specific.add(chatMessage);


        chatMessages.add(chatMessage);


        Log.i("Checkingsize", chatMessages.size() + "");


        if (chatMessage.getUserType() == UserType.MULTIOPTIONS) {
            edittextdisable();
            listAdapter = new ChatListAdapter(chatMessages, this, this);

        } else {
            edittextenable();
            listAdapter = new ChatListAdapter(chatMessages, this);
        }
        mListview.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();


        // EventBus.getDefault().register(this);

/*
        mstorage = new storagemodel(this);
        mstorage.sendvaluesupdated(this, specific);
*/


        if (chatMessage.getUserType() == UserType.COOL) {

            if (constant.getSoundon()) {

                int size = chatMessages.size() - 1;
                speakfunction(chatMessages.get(size).getMessageText());
                Lastspeech = chatMessages.get(size).getMessageText();

            }

        }
        if (chatMessage.getUserType() == UserType.MULTIOPTIONS) {

            if (constant.getSoundon()) {

                int size = chatMessages.size() - 1;
                speakfunction(chatMessages.get(size).getMessageText());

                Lastspeech = chatMessages.get(size).getMessageText();

            }

        }


    }

    @Override
    protected void onRestart() {

        Toast.makeText(this, "Restarted", Toast.LENGTH_SHORT).show();
        super.onRestart();
    }

    public void sendmessageinnormal(ChatMessage chatMessage) {


/*
        specific = new ArrayList<>();
        specific.add(chatMessage);
*/


        chatMessages.add(chatMessage);


        Log.i("Checkingsize", chatMessages.size() + "");


        if (chatMessage.getUserType() == UserType.MULTIOPTIONS) {
            listAdapter = new ChatListAdapter(chatMessages, this, this);

        } else {
            listAdapter = new ChatListAdapter(chatMessages, this);
        }
        mListview.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();


        // EventBus.getDefault().register(this);

/*
        mstorage = new storagemodel(this);
        mstorage.sendvaluesupdated(this, specific);
*/


        if (chatMessage.getUserType() == UserType.COOL) {

            if (constant.getSoundon()) {

                int size = chatMessages.size() - 1;
                speakfunction(chatMessages.get(size).getMessageText());
                Lastspeech = chatMessages.get(size).getMessageText();

            }

        }


    }


    @Subscribe
    public void onEvent(Customloginresponsemessage custommessageevent) {
        //Toast.makeText(this, "Event fired" + custommessageevent.getCustommessage(), Toast.LENGTH_SHORT).show();
        Toast.makeText(this, custommessageevent.getUseremail(), Toast.LENGTH_SHORT).show();
        // mchatmodel.presentstring(custommessageevent.getUseremail(),ChatActivity.this);

    }


    private void speakfunction(String messageText) {
        t1.speak(messageText, TextToSpeech.QUEUE_FLUSH, null);
        t1.setLanguage(Locale.US);

    }


    @Override
    public void error() {

        Toast.makeText(this, "error in listening", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void success(String result) {


        if (CheckNetwork.isInternetAvailable(getApplicationContext())) {
            mchatmodel.presentstring(result, ChatActivity.this);
        } else {
            Toast.makeText(this, "your request could not submitted as no network available", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onDestroy() {

        mSocket.off();
        mSocket.disconnect();
        super.onDestroy();
    }

    @Override
    public void begining() {

        Toast.makeText(this, "Ready to Start", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void speakout() {
        if (chatMessages.size() == 0) {

        } else {
            speakfunction(chatMessages.get(chatMessages.size() - 1).getMessageText());
        }


    }


    @Override
    public void successstore(ArrayList<ChatMessage> chatMessage) {

        this.chatMessages = chatMessage;

        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                Log.i("Cdkfj", chatMessages.get(0).getMessageText());

                listAdapter = new ChatListAdapter(chatMessages, ChatActivity.this, ChatActivity.this);

                mListview.setAdapter(listAdapter);
                listAdapter.notifyDataSetChanged();


                // close this activity


            }
        }, 500);


    }/**/

    @Override
    public void end(String text) {
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                Intent i = new Intent(ChatActivity.this, NewHome.class);
                startActivity(i);

                // close this activity

            }
        }, 500);

    }


    @Override
    public void retry(String text) {

        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
/*
        Snackbar.make(findViewById(android.R.id.content), text,
        Snackbar.make(findViewById(android.R.id.content), text,
                Snackbar.LENGTH_SHORT)
                .show();
*/

    }

    @Override
    public void progressdisplayon() {

        progressDialog.setMessage("Loading Please wait...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();


    }

    @Override
    public void progressdisplayoff() {
        progressDialog.dismiss();
    }


    public void edittextenable() {
        bottomlayout.setVisibility(View.VISIBLE);
        username.setFocusable(true);
    }

    @Override
    public void edittextdisable() {
        bottomlayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void manualtransfer() {


        edittextdisable();
        constant.setSocketconection(true);
        //Socketconection = true;


    }


    @Override
    public void failurestore() {

        progress.dismiss();
        Toast.makeText(this, "no data", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void progressstarted() {

        progress.show();
        progress.setMessage("Loading");

    }

    @Override
    public void progresscompleted() {

        progress.dismiss();

    }


    @Override
    public void onInit(int i) {
        if (i != TextToSpeech.ERROR) {
            if (chatMessages.size() == 0) {

            } else {

                speakfunction(Lastspeech);


            }
        }
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        if (isConnected) {
            message = "Good! Connected to Internet";
            Log.i("Checking", message);

        } else {
            message = "Sorry! Not connected to internet";
            Log.i("Checking", message);
        }

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.muteimage) {

            //Soundon = false;
            constant.setSoundon(false);
            mute.setVisibility(View.INVISIBLE);
            unmute.setVisibility(View.VISIBLE);
        } else if (v.getId() == R.id.unmuteimage) {

            unmute.setVisibility(View.INVISIBLE);
            mute.setVisibility(View.VISIBLE);
            // Soundon = true;
            constant.setSoundon(false);
        }

    }

    @Override
    public void onClick(String value, int pos) {

        /*Toast.makeText(this,value, Toast.LENGTH_SHORT).show();*/


        if (pos == chatMessages.size() - 1) {
            mchatmodel.presentstring(value, this);
        } else {
            Toast.makeText(this, "your request not accepted", Toast.LENGTH_SHORT).show();
        }

    }

    private class MyBackgroundAsyncservice extends AsyncTask<Void, Void, String> {

        private int status;
        private InputStream is;
        private String json;
        Constant constant;


        @Override
        protected void onPreExecute() {
            constant = new Constant();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            // TODO Auto-generated method stub
            return postData();

        }

        protected void onPostExecute(String result) {


            // keyboardrequest = false;
            constant.setKeyboardrequest(false);
            // pb.setVisibility(View.GONE);


           /* Toast.makeText(ChatActivity.this, result, Toast.LENGTH_SHORT).show();
            Log.i("Responsemongo", result);
            JSONObject js = null;
            try {
                js = new JSONObject(result);


                if (js.optString("msg_text") != null || !js.optString("msg_text").isEmpty()) {

                    if (js.optString("message").equals("UserAgentMessage successfully Created!")) {

                        Toast.makeText(ChatActivity.this, "Server accepted your request", Toast.LENGTH_SHORT).show();

                    } else {

*//*
                        final ChatMessage messagerece = new ChatMessage();
                        messagerece.setMessageText(js.optString("msg_text")); // 10 spaces;
                        messagerece.setUserType(UserType.COOL);
                        messagerece.setMessageTime(new Date().getTime());
                        sendmessage(messagerece);
*//*

                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }*/

        }

        protected void onProgressUpdate(Void... progress) {
            // pb.setProgress(progress[0]);
        }

        private String postData() {
            // Create a new HttpClient and Post Header

            String text = username.getText().toString();
            HttpClient httpclient = new DefaultHttpClient();
            // HttpPost httppost = new HttpPost("http://192.168.0.187:3000/api/user_agent_message");
            HttpPost httppost = new HttpPost("http://139.59.32.187:3000/api/user_agent_message");

            try {
                // Add your data
/*
                List<T> nameValuePairs = new ArrayList<T>();
                nameValuePairs.add(new BasicNameValuePair("msg_agent_id", constant.CliendId));
                nameValuePairs.add(new BasicNameValuePair("msg_flow",1));
                nameValuePairs.add(new BasicNameValuePair("msg_session_id", sessionkey));
                nameValuePairs.add(new BasicNameValuePair("msg_text", username.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("msg_status",1));
*/

                String json = "";
                JSONObject object = new JSONObject();
                object.accumulate("msg_agent_id", constant.CliendId);
                object.accumulate("msg_flow", 1);
                object.accumulate("msg_session_id", constant.getSessionkey());
                object.accumulate("msg_text", text);
                object.accumulate("msg_status", 1);

                //  httppost.setEntity(new UrlEncodedFormEntity(object.toString()));

                json = object.toString();

                StringEntity se = new StringEntity(json);

                httppost.setEntity(se);
                httppost.setHeader("Accept", "application/json");
                httppost.setHeader("Content-type", "application/json");
                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                status = response.getStatusLine().getStatusCode();
                HttpEntity httpEntity = response.getEntity();
                is = httpEntity.getContent();


                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            is, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "n");
                    }
                    is.close();
                    json = sb.toString();

                    return json;
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }


            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return json;
        }

    }

}
