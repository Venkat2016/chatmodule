package com.example.welcome.chatprojectinmvpmodel.Activity;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.welcome.chatprojectinmvpmodel.R;

import java.security.KeyStore;
import java.security.KeyStoreException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class Fingerprintauthenticatonactivity extends AppCompatActivity {


    private String KEY_NAME = "somekeyname";
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fingerprintauthenticatonactivity);

        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

        if(!fingerprintManager.isHardwareDetected())
        {

            Log.i("Hardware", "Fingerprint not detected");
            return;

        }
        else
        {
            Log.i("Hardware", "Fingerprint detected");
        }

        if(!keyguardManager.isKeyguardSecure())
        {
            Log.i("Hardware", "Keyguard not enabled");
            return;
        }

        KeyStore keyStore;

        try
        {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (KeyStoreException e) {
            Log.i("Keystore", e.getMessage());
            return;
        }

        KeyGenerator keyGenerator = null;
        try
        {

            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES,"AndroidKeyStore");
        }catch (Exception e)
        {
            Log.i("KeyGenerator", e.getMessage());

        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {

            Toast.makeText(this, "Not enabled", Toast.LENGTH_SHORT).show();
        }else{
            // Check whether at least one fingerprint is registered

        }
        try
        {
            keyStore.load(null);
            keyGenerator.init(new KeyGenParameterSpec.Builder(KEY_NAME,KeyProperties.PURPOSE_ENCRYPT|KeyProperties.PURPOSE_DECRYPT).setBlockModes(KeyProperties.BLOCK_MODE_CBC).setUserAuthenticationRequired(true).setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7).build());
            keyGenerator.generateKey();
        }catch (Exception e)
        {
            Log.i("Generating Keys", e.getMessage());
        }

        Cipher cipher = null;
        try
        {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        }catch (Exception e)
        {
            Log.i("Cipher", e.getMessage());
        }
        try
        {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME, null);
            cipher.init(Cipher.ENCRYPT_MODE,key);
        }catch (Exception e)
        {
            Log.i("Secret Key", e.getMessage());
        }

        FingerprintManager.CryptoObject cryptoobject = new FingerprintManager.CryptoObject(cipher);

        CancellationSignal cancellationsignal = new CancellationSignal();
        fingerprintManager.authenticate(cryptoobject,cancellationsignal,0,new AuthenticationHandler(this),null);

    }
}
