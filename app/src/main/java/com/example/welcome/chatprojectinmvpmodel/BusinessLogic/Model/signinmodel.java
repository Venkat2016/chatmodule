package com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model;

import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Presenter.signinpresenter;
import com.example.welcome.chatprojectinmvpmodel.R;
import com.example.welcome.chatprojectinmvpmodel.Retrofitclasses.Pathwithgetstaticclass;
import com.example.welcome.chatprojectinmvpmodel.Retrofitinterface.APIInterface;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.signinview;
import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import static com.example.welcome.chatprojectinmvpmodel.Activity.ChatActivity.URL;

/**
 * Created by welcome on 21-09-2017.
 */

public class signinmodel implements signinpresenter {


    private signinview signinview;

    private APIInterface service;
    private Call<ResponseBody> mCall;

    public signinmodel(signinview signinview) {
        this.signinview = signinview;
    }



    @Override
    public void customercredentials(String username, String password) {


        if (userpasswordvalidation(password)) {
            if (usernamevalidation(username)) {

                /*signinview.showprogressview();*/
                /*new Handler().postDelayed(new Runnable() {*/
                /*    @Override*/
                /*    public void run() {*/
                /*               signinview.hideprogressview();*/
                /*    }*/
                /*}, 2000);*/
                signinview.signinofflinevalidationsucess(username,password);


                //  signinview.showprogressview();

                // establishnetworkconnection(username,password);
            }
        }


    }


    public void establishnetworkconnection(String username, String password) {

        //Establishing network connection


        service = Pathwithgetstaticclass.getClient().create(APIInterface.class);

        mCall = service.signinapi(username, password);

        mCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                signinview.hideprogressview();
                if (response.isSuccessful()) {
                    signinview.signinsuccess();
                } else {
                    try {
                        signinview.networkfailure(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                signinview.networkfailure("Some thing went wrong on network side! Please try again");

            }
        });


    }



    private Boolean usernamevalidation(String username) {


        if (username.isEmpty()) {
            signinview.signinfailure(R.string.Mobilevalidationerror);
            return false;
        } else if (username.length() != 10) {
            signinview.signinfailure(R.string.Mobilevalidationerror);
            return false;
        } else {
            return true;
        }


    }

    private Boolean userpasswordvalidation(String password) {


        if (password.isEmpty()) {
            signinview.signinfailure(R.string.Passwordvalidationerror);
            return false;
        } else if (!(password.length() >= 6)) {
            signinview.signinfailure(R.string.Passwordlengtherror);
            return false;
        } else {
            return true;
        }


    }


}
