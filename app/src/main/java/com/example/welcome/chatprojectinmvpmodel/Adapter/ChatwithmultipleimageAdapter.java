package com.example.welcome.chatprojectinmvpmodel.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.welcome.chatprojectinmvpmodel.R;
import com.squareup.picasso.Picasso;

import java.util.Date;



/**
 * Created by VENKATESH on 1/23/2017.
 */
public class ChatwithmultipleimageAdapter extends RecyclerView.Adapter {
    Context context;

    //ArrayList<String> list = new ArrayList<String>();
    String[] flowerlist;
    Date dateformat;
    private String finalString,time;


    public ChatwithmultipleimageAdapter(Context context, String[] flowerList) {
        this.context = context;
        this.flowerlist = flowerList;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.card_about_1, parent, false);
        Item item = new Item(row);

        return item;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        Picasso.with(context).load(flowerlist[position]).into(((Item)holder).imageView);

    }



    @Override
    public int getItemCount() {
       // Toast.makeText(context, flowerlist.size()+"", Toast.LENGTH_SHORT).show();
        return flowerlist.length;
    }
    public class Item extends RecyclerView.ViewHolder {
        TextView eventdatetview;ImageView imageView;
        TextView eventtitletview,eventvenueview;
        public Item(View itemView) {
            super(itemView);
           // eventdatetview = (TextView) itemView.findViewById(R.id.date);
            imageView = (ImageView) itemView.findViewById(R.id.img_main_card_1);
            //eventtitletview = (TextView) itemView.findViewById(R.id.title);
           // eventvenueview = (TextView) itemView.findViewById(R.id.venue);
            //favoriteImg = (ImageView) itemView.findViewById(R.id.imgbtn_favorite);

        }
    }




}

