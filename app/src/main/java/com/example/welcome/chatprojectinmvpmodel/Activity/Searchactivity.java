package com.example.welcome.chatprojectinmvpmodel.Activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.example.welcome.chatprojectinmvpmodel.Adapter.SimpleAdapter;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model.storagemodel;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.storageview;
import com.example.welcome.chatprojectinmvpmodel.Models.ChatMessage;
import com.example.welcome.chatprojectinmvpmodel.R;

import java.util.ArrayList;
import java.util.List;


public class Searchactivity extends AppCompatActivity implements storageview {

    EditText search;
    private storagemodel mstoragemodel;
    private RecyclerView mRecyclerView;
    private SimpleAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchactivity);

        search = (EditText) findViewById(R.id.search_xml);
        mRecyclerView = (RecyclerView) findViewById(R.id.listfilter);


        searchingitemsgetfromdb();
    }

    private void searchingitemsgetfromdb() {

        mstoragemodel = new storagemodel(this);

        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {

                mstoragemodel.getvaluesfromdb(Searchactivity.this);


            }
        }, 500);

    }

    private void addsearchlistener(final ArrayList<ChatMessage> chatMessages) {
        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {

                query = query.toString().toLowerCase();

                final List<String> filteredList = new ArrayList<>();

                for (int i = 0; i < chatMessages.size(); i++) {

                    final String text = chatMessages.get(i).getMessageText();

                    if (text.contains(query)) {

                        filteredList.add(chatMessages.get(i).getMessageText()+":"+chatMessages.get(i).getMessagedate());

                    }
                }


                mRecyclerView.setLayoutManager(new LinearLayoutManager(Searchactivity.this));
                mAdapter = new SimpleAdapter(filteredList, Searchactivity.this);
                mRecyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();  // data set changed
            }
        });
    }

    @Override
    public void successstore(ArrayList<ChatMessage> chatMessages) {

        addsearchlistener(chatMessages);

    }

    @Override
    public void failurestore() {

    }

    @Override
    public void progressstarted() {

    }

    @Override
    public void progresscompleted() {

    }
}
