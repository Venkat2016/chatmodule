package com.example.welcome.chatprojectinmvpmodel.Activity;

import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.welcome.chatprojectinmvpmodel.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

public class Universalimageloader extends AppCompatActivity {

    ImageView imageView;
    ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_universalimageloader);

        imageView = (ImageView) findViewById(R.id.imageView);



        ImageLoader imageLoader = ImageLoader.getInstance();
        String imageUri = "http://www.ssaurel.com/tmp/logo_ssaurel.png";
        imageLoader.loadImage(imageUri, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                // use the loaded image …

                Toast.makeText(Universalimageloader.this, "Cool its downloaded", Toast.LENGTH_SHORT).show();
            }
        });
        imageLoader.displayImage(imageUri, imageView);


      //  imageLoader.displayImage("https://lh3.googleusercontent.com/iKEdGSVTh06yriQ3o6vrW_nn-SMF8yxFMXZ0Hem32hMkt2Jxg89pnbf9BFgB9YaYj0o=h310", imageView);

    }
}
