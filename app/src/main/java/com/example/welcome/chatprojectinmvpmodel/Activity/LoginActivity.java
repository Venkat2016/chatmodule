package com.example.welcome.chatprojectinmvpmodel.Activity;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model.signinmodel;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.signinview;
import com.example.welcome.chatprojectinmvpmodel.R;

import static com.example.welcome.chatprojectinmvpmodel.Others.AesScript.encrypt;


public class LoginActivity extends AppCompatActivity implements signinview {

    private signinmodel msigninmodel;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        progressBar = (ProgressBar) findViewById(R.id.determinateBar);

        msigninmodel = new signinmodel(this);

        msigninmodel.customercredentials("980649860", "Hellossl");



    }



    private void snackbarmethod(String message) {
        Snackbar.make(findViewById(android.R.id.content), message,
                Snackbar.LENGTH_SHORT).show();

    }

    @Override
    public void signinofflinevalidationsucess(final String username, final String password) {


        try {


            Toast.makeText(this,encrypt(username), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }


/*
        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {

                    msigninmodel.establishnetworkconnection(username,password);

            }
        }, 1000);
*/


    }



    @Override
    public void signinsuccess() {

        //Total signin procedure completed

    }

    @Override
    public void signinfailure(int message) {

        snackbarmethod(getResources().getString(message));


    }

    @Override
    public void networkfailure(String message) {

        snackbarmethod(message);

    }

    @Override
    public void showprogressview() {

        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideprogressview() {

        progressBar.setVisibility(View.INVISIBLE);
    }

}
