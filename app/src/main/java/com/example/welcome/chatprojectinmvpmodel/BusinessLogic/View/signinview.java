package com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View;

/**
 * Created by welcome on 21-09-2017.
 */

public interface signinview {

    void signinofflinevalidationsucess(String username, String password);

    void signinsuccess();

    void signinfailure(int message);



    void networkfailure(String message);

    void showprogressview();

    void hideprogressview();


}
