package com.example.welcome.chatprojectinmvpmodel.Others;/*
package in.co.madhur.chatbubblesdemo;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;

import in.co.madhur.chatbubblesdemo.Retrofitinterface.APIInterface;
import in.co.madhur.chatbubblesdemo.model.ChatMessage;
import in.co.madhur.chatbubblesdemo.model.Status;
import in.co.madhur.chatbubblesdemo.model.UserType;
import in.co.madhur.chatbubblesdemo.widgets.Emoji;
import in.co.madhur.chatbubblesdemo.widgets.EmojiView;
import in.co.madhur.chatbubblesdemo.Retrofitclasses.Pathwithgetstaticclass;
import in.co.madhur.chatbubblesdemo.widgets.SizeNotifierRelativeLayout;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends ActionBarActivity implements NotificationCenter.NotificationCenterDelegate,TextToSpeech.OnInitListener,RecognitionListener {

    private static final int REQ_CODE_SPEECH_INPUT = 100;
    private ListView chatListView;
    private EditText chatEditText1;
    private ArrayList<ChatMessage> chatMessages;
    private ImageView enterChatView1, emojiButton,userfemale,usermale;
    private EmojiView emojiView;
    private ImageView entervoice;
    private ChatListAdapter listAdapter;
    private SizeNotifierRelativeLayout sizeNotifierRelativeLayout;
    private boolean showingEmoji;
    private int keyboardHeight;
    private boolean keyboardVisible;
    private WindowManager.LayoutParams windowLayoutParams;
    private Spinner voiceselectionspinner_xml;
    TextToSpeech t1;
    Locale locselection;
    ProgressDialog progressdialgo;



    private EditText.OnKeyListener keyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {

            // If the event is a key-down event on the "enter" button
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                // Perform action on key press

                EditText editText = (EditText) v;

                if(v==chatEditText1)
                {
                    sendMessage(editText.getText().toString(), UserType.OTHER);
                }

                chatEditText1.setText("");

                return true;
            }
            return false;

        }
    };

    private ImageView.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(v==enterChatView1)
            {
                sendMessage(chatEditText1.getText().toString(), UserType.OTHER);
            }

            chatEditText1.setText("");

        }
    };

    private final TextWatcher watcher1 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (chatEditText1.getText().toString().equals("")) {
                entervoice.setVisibility(View.VISIBLE);
                enterChatView1.setVisibility(View.INVISIBLE);

            } else {

                entervoice.setVisibility(View.INVISIBLE);
                enterChatView1.setVisibility(View.VISIBLE);
                enterChatView1.setImageResource(R.drawable.input_send);

            }
        }


        @Override
        public void afterTextChanged(Editable editable) {
            enterChatView1.setImageResource(R.drawable.input_send);
*/
/*            if(editable.length()==0){
                enterChatView1.setImageResource(R.drawable.input_send);
            }else{
                enterChatView1.setImageResource(R.drawable.input_send);
            }*//*

        }
    };
    private Handler handler;
    private Call<ResponseBody> mCall;
    JsonObject object;
    JsonObject obj2;
    JsonArray arr1;
    JsonObject obj3;
    private APIInterface service;
    private boolean isfirst = true;
    private SpeechRecognizer speech = null;
    private Intent recognizerIntent;
    private int session;
    private int count=1;
    private ChatMessage message;
    private FrameLayout framelistening;


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        handler = new Handler();
        AndroidUtilities.statusBarHeight = getStatusBarHeight();

        Random r = new Random();
        session = (r.nextInt(80) + 65);

        locselection = Locale.US;
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.background));

        userfemale = (ImageView) findViewById(R.id.userfemale);
        usermale = (ImageView) findViewById(R.id.usermale);
        chatMessages = new ArrayList<>();

        chatListView = (ListView) findViewById(R.id.chat_list_view);
       // voiceselectionspinner_xml = (Spinner) findViewById(R.id.voiceselectionspinner_xml);
        entervoice = (ImageView) findViewById(R.id.enter_voice);
        chatEditText1 = (EditText) findViewById(R.id.chat_edit_text1);
        enterChatView1 = (ImageView) findViewById(R.id.enter_chat1);
        framelistening = (FrameLayout) findViewById(R.id.framelistening);

        t1 = new TextToSpeech(getApplicationContext(), this, "com.google.android.tts");


        usermale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(MainActivity.this, Languagelisted.class);
                startActivity(i);
*/
/*
                progressdialgo = new ProgressDialog(MainActivity.this);
                progressdialgo.setMessage("Loading");
                progressdialgo.show();
                final Runnable r = new Runnable() {
                    public void run() {

                        locselection = Locale.ENGLISH;
                        progressdialgo.dismiss();

                    }
                };

                handler.postDelayed(r, 1000);
*//*



            }
        });


        userfemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressdialgo = new ProgressDialog(MainActivity.this);
                progressdialgo.setMessage("Loading");
                progressdialgo.show();
                final Runnable r = new Runnable() {
                    public void run() {

                        locselection = Locale.CANADA_FRENCH;
                        progressdialgo.dismiss();

                    }
                };

                handler.postDelayed(r, 1000);


            }
        });

*/
/*
        ArrayAdapter<String> arrayadapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.voices));
        voiceselectionspinner_xml.setAdapter(arrayadapter);

        voiceselectionspinner_xml.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if(i==0)
                {

                            locselection = Locale.CANADA_FRENCH;

                }
                else{
                    locselection = Locale.ENGLISH;
                }
            }
*//*


*/
/*
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                locselection = Locale.CANADA_FRENCH;
            }
        });
*//*

*/
/*
        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {

            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);


                }
            }
        });
*//*


        // Hide the emoji on click of edit text
        chatEditText1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (showingEmoji)
                    hideEmojiPopup();
            }
        });

        final Timer timer = new Timer();
        entervoice.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch(motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        // You can call a thread here which will keep increasing the time

                        speechsynthesis();
                        speech.startListening(recognizerIntent);

                        return true;
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        // Stop the thread here
                        Toast.makeText(MainActivity.this, "Stop listening", Toast.LENGTH_SHORT).show();
                        speech.stopListening();
                        return true;
                }

                return false;
            }
        });

*/
/*
        entervoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(count%2==0) {
                    Toast.makeText(MainActivity.this, "Stopped Listening", Toast.LENGTH_SHORT).show();
                    count++;
                    speech.stopListening();
                }


                else
                {
                    startVoiceInput();
                }
            }
        });
*//*


        emojiButton = (ImageView)findViewById(R.id.emojiButton);

       */
/* emojiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEmojiPopup(!showingEmoji);
            }
        });
*//*



        listAdapter = new ChatListAdapter(chatMessages, this);

        chatListView.setAdapter(listAdapter);

        chatEditText1.setOnKeyListener(keyListener);

        enterChatView1.setOnClickListener(clickListener);

        chatEditText1.addTextChangedListener(watcher1);

        //sizeNotifierRelativeLayout = (SizeNotifierRelativeLayout) findViewById(R.id.chat_layout);
        //sizeNotifierRelativeLayout.delegate = this;

        NotificationCenter.getInstance().addObserver(this, NotificationCenter.emojiDidLoaded);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void startVoiceInput() {
*/
/*
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Hello, How can I help you?");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {

        }
*//*

        count++;
        speech = SpeechRecognizer.createSpeechRecognizer(this);
        speech.setRecognitionListener(MainActivity.this);
        recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE,
                "en");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                this.getPackageName());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);
        speech.startListening(recognizerIntent);

    }

    public void speechsynthesis()
    {
        speech = SpeechRecognizer.createSpeechRecognizer(this);
        speech.setRecognitionListener(MainActivity.this);
        recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE,
                "en");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                this.getPackageName());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);


    }

    private void apicaliing() {
        service = Pathwithgetstaticclass.getClient().create(APIInterface.class);


        JsonObject object = new JsonObject();
        JsonObject obj2 = new JsonObject();
        JsonArray arr1 = new JsonArray();


        JsonObject obj3 = new JsonObject();


        object.addProperty("session", session+"");

        object.add("result", obj2);

        obj2.addProperty("resulttype", "Partial");

        obj2.add("alts", arr1);


        obj3.addProperty("confidence", "1");
        obj3.addProperty("transcript", chatEditText1.getText().toString());


        arr1.add(obj3);


        Log.i("Checkingdfj", object.toString());


        mCall = service.getpython(object);


        mCall.enqueue(new Callback<ResponseBody>() {


            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                // this method will execute when reaches the status code 200
                if (response.code() == 200) {

                    // mProgressDialog.dismiss();

                    JSONObject js;

                    try {




                            js = new JSONObject(response.body().string());
                            String messagetext = js.getString("text");


                        message.setMessageStatus(Status.READ);

                        final ChatMessage message = new ChatMessage();
                        message.setMessageStatus(Status.SENT);
                        message.setMessageText(messagetext); // 10 spaces;
                        message.setUserType(UserType.SELF);
                        message.setMessageTime(new Date().getTime());


                            t1.speak(messagetext, TextToSpeech.QUEUE_FLUSH, null);
                            t1.setLanguage(locselection);
                        chatMessages.add(message);
                            listAdapter.notifyDataSetChanged();
                            isfirst = false;
                        */
/*
                        else {
                            LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                            View dialogLayout = inflater.inflate(R.layout.customalertdialog_layout, null);
                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                            builder.setView(dialogLayout);
                            TextView indtruction = (TextView) dialogLayout.findViewById(R.id.text_instruction);
                            final EditText userInput = (EditText) dialogLayout.findViewById(R.id.user_input);
                            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated method stub
                                    dialog.dismiss();
                                }


                            });
                            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated method stub
                                    String userInputContent = userInput.getText().toString();
                                   // textFromDialog.setText(userInputContent);
                                }
                            });
                            AlertDialog customAlertDialog = builder.create();
                            customAlertDialog.show();
                        }*//*

                    }catch (Exception e) {

                        Toast.makeText(MainActivity.this,e.toString(), Toast.LENGTH_SHORT).show();
                    }


                }
*/
/*
                        MainActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                listAdapter.notifyDataSetChanged();
                            }
                        });
*//*









                    // Toast.makeText(Testingactivity.this, "", Toast.LENGTH_SHORT).show();
                 else {

                    try {
                        Log.i("Checkingrespnon", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //  constant.alertdialogwithcallresponse(Splashscreen.this, getResources().getString(R.string.Experiencetechnical), call);

                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //  mProgressDialog.dismiss();
                if (t instanceof ConnectException) {
                    Log.i("Checksocket", "Connect");
                }

                if (t instanceof SocketTimeoutException) {
                    Log.i("Checksocket", "Socket");
                }



            }

        });
    }

        @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    chatEditText1.setText(result.get(0));
                    chatEditText1.setVisibility(View.INVISIBLE);
                    sendMessage(chatEditText1.getText().toString(), UserType.OTHER);
                    chatEditText1.setText("");
                    chatEditText1.setVisibility(View.VISIBLE);
                }
                break;
            }

        }
    }


    private void sendMessage(final String messageText, final UserType userType)
    {
        if(messageText.trim().length()==0)
            return;

        message = new ChatMessage();
        message.setMessageStatus(Status.SENT);
        message.setMessageText(messageText);
        message.setUserType(UserType.COOL);
        message.setMessageTime(new Date().getTime());
        chatMessages.add(message);

        if(listAdapter!=null)
            listAdapter.notifyDataSetChanged();

        if(isfirst) {
            apicaliing();
        }
        else {
            apisecondcaliing();
        }

        // Mark message as delivered after one second

*/
/*
        final ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);

        exec.schedule(new Runnable() {
            @Override
            public void run() {
                message.setMessageStatus(Status.READ);

                final ChatMessage message = new ChatMessage();
                message.setMessageStatus(Status.SENT);
                message.setMessageText(messageText); // 10 spaces;
                message.setUserType(UserType.SELF);
                message.setMessageTime(new Date().getTime());
                t1.speak(messageText, TextToSpeech.QUEUE_FLUSH, null);
                t1.setLanguage(locselection);
                message.setMessageStatus(Status.READ);
                chatMessages.add(message);




                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        listAdapter.notifyDataSetChanged();
                    }
                });


            }
        }, 1, TimeUnit.SECONDS);
*//*


    }

    private void apisecondcaliing() {

        isfirst = false;

        service = Pathwithgetstaticclass.getClient().create(APIInterface.class);


        JsonObject object = new JsonObject();
        JsonObject obj2 = new JsonObject();
        JsonArray arr1 = new JsonArray();


        JsonObject obj3 = new JsonObject();


        object.addProperty("session",session+"");

        object.add("result",obj2);

        obj2.addProperty("resulttype","Partial");

        obj2.add("alts",arr1);



        obj3.addProperty("confidence","1");
        obj3.addProperty("transcript",chatEditText1.getText().toString());



        arr1.add(obj3);



        Log.i("Checkingdfj", object.toString());


        mCall = service.getpythonnext(object);


        mCall.enqueue(new Callback<ResponseBody>() {


            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                // this method will execute when reaches the status code 200
                if (response.code() == 200) {

                    // mProgressDialog.dismiss();


                    JSONObject js = null;

                    try {
                       // Log.i("Checkingrespnon", response.body().string());


                        js = new JSONObject(response.body().string());
                        String messagetext = js.getString("text");
                        Toast.makeText(MainActivity.this, messagetext, Toast.LENGTH_SHORT).show();


                        message.setMessageStatus(Status.READ);

                        final ChatMessage message = new ChatMessage();
                        message.setMessageStatus(Status.SENT);
                        message.setMessageText(messagetext); // 10 spaces;
                        message.setUserType(UserType.SELF);
                        message.setMessageTime(new Date().getTime());
                        String messageend = js.optString("final");

                        if(messageend.equals("true"))
                        {
                            Toast.makeText(MainActivity.this,"Good Bye!!", Toast.LENGTH_SHORT).show();
                            finish();

                        }
                        else {

                            Toast.makeText(MainActivity.this, messageend, Toast.LENGTH_SHORT).show();


                            t1.speak(messagetext, TextToSpeech.QUEUE_FLUSH, null);
                            t1.setLanguage(locselection);

                            chatMessages.add(message);
                            listAdapter.notifyDataSetChanged();

                        }
*/
/*
                        MainActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                listAdapter.notifyDataSetChanged();
                            }
                        });
*//*



                    } catch (Exception e) {

                    }


                }
                    // Toast.makeText(Testingactivity.this, "", Toast.LENGTH_SHORT).show();
                 else {

                    try {
                        Log.i("Checkingrespnon", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //  constant.alertdialogwithcallresponse(Splashscreen.this, getResources().getString(R.string.Experiencetechnical), call);

                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //  mProgressDialog.dismiss();
                if (t instanceof ConnectException) {
                    Log.i("Checksocket", "Connect");
                }

                if (t instanceof SocketTimeoutException) {
                    Log.i("Checksocket", "Socket");
                }


            }

        });


    }

    private Activity getActivity()
    {
        return this;
    }

    //@Override
    public void onSizeChanged(int height) {

        Rect localRect = new Rect();
        getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(localRect);

        WindowManager wm = (WindowManager) App.getInstance().getSystemService(Activity.WINDOW_SERVICE);
        if (wm == null || wm.getDefaultDisplay() == null) {
            return;
        }


        if (height > AndroidUtilities.dp(50) && keyboardVisible) {
            keyboardHeight = height;
            App.getInstance().getSharedPreferences("emoji", 0).edit().putInt("kbd_height", keyboardHeight).commit();
        }


        if (showingEmoji) {
            int newHeight = 0;

            newHeight = keyboardHeight;

            if (windowLayoutParams.width != AndroidUtilities.displaySize.x || windowLayoutParams.height != newHeight) {
                windowLayoutParams.width = AndroidUtilities.displaySize.x;
                windowLayoutParams.height = newHeight;

                wm.updateViewLayout(emojiView, windowLayoutParams);
                if (!keyboardVisible) {
                    sizeNotifierRelativeLayout.post(new Runnable() {
                        @Override
                        public void run() {
                            if (sizeNotifierRelativeLayout != null) {
                                sizeNotifierRelativeLayout.setPadding(0, 0, 0, windowLayoutParams.height);
                                sizeNotifierRelativeLayout.requestLayout();
                            }
                        }
                    });
                }
            }
        }


        boolean oldValue = keyboardVisible;
        keyboardVisible = height > 0;
        if (keyboardVisible && sizeNotifierRelativeLayout.getPaddingBottom() > 0) {
            showEmojiPopup(false);
        } else if (!keyboardVisible && keyboardVisible != oldValue && showingEmoji) {
            showEmojiPopup(false);
        }

    }

    */
/**
     * Show or hide the emoji popup
     *
     * @param show
     *//*

    private void showEmojiPopup(boolean show) {
        showingEmoji = show;

        if (show) {
            if (emojiView == null) {
                if (getActivity() == null) {
                    return;
                }
                emojiView = new EmojiView(getActivity());

                emojiView.setListener(new EmojiView.Listener() {
                    public void onBackspace() {
                        chatEditText1.dispatchKeyEvent(new KeyEvent(0, 67));
                    }

                    public void onEmojiSelected(String symbol) {
                        int i = chatEditText1.getSelectionEnd();
                        if (i < 0) {
                            i = 0;
                        }
                        try {
                            CharSequence localCharSequence = Emoji.replaceEmoji(symbol, chatEditText1.getPaint().getFontMetricsInt(), AndroidUtilities.dp(20));
                            chatEditText1.setText(chatEditText1.getText().insert(i, localCharSequence));
                            int j = i + localCharSequence.length();
                            chatEditText1.setSelection(j, j);
                        } catch (Exception e) {
                            Log.e(Constants.TAG, "Error showing emoji");
                        }
                    }
                });


                windowLayoutParams = new WindowManager.LayoutParams();
                windowLayoutParams.gravity = Gravity.BOTTOM | Gravity.LEFT;
                if (Build.VERSION.SDK_INT >= 21) {
                    windowLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
                } else {
                    windowLayoutParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_PANEL;
                    windowLayoutParams.token = getActivity().getWindow().getDecorView().getWindowToken();
                }
                windowLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
            }

            final int currentHeight;

            if (keyboardHeight <= 0)
                keyboardHeight = App.getInstance().getSharedPreferences("emoji", 0).getInt("kbd_height", AndroidUtilities.dp(200));

            currentHeight = keyboardHeight;

            WindowManager wm = (WindowManager) App.getInstance().getSystemService(Activity.WINDOW_SERVICE);

            windowLayoutParams.height = currentHeight;
            windowLayoutParams.width = AndroidUtilities.displaySize.x;

            try {
                if (emojiView.getParent() != null) {
                    wm.removeViewImmediate(emojiView);
                }
            } catch (Exception e) {
                Log.e(Constants.TAG, e.getMessage());
            }

            try {
                wm.addView(emojiView, windowLayoutParams);
            } catch (Exception e) {
                Log.e(Constants.TAG, e.getMessage());
                return;
            }

            if (!keyboardVisible) {
                if (sizeNotifierRelativeLayout != null) {
                    sizeNotifierRelativeLayout.setPadding(0, 0, 0, currentHeight);
                }

                return;
            }

        }
        else {
            removeEmojiWindow();
            if (sizeNotifierRelativeLayout != null) {
                sizeNotifierRelativeLayout.post(new Runnable() {
                    public void run() {
                        if (sizeNotifierRelativeLayout != null) {
                            sizeNotifierRelativeLayout.setPadding(0, 0, 0, 0);
                        }
                    }
                });
            }
        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        NotificationCenter.getInstance().removeObserver(this, NotificationCenter.emojiDidLoaded);
    }

    */
/**
     * Get the system status bar height
     * @return
     *//*

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    protected void onPause() {
        super.onPause();

        hideEmojiPopup();
    }

    */
/**
     * Remove emoji window
     *//*

    private void removeEmojiWindow() {
        if (emojiView == null) {
            return;
        }
        try {
            if (emojiView.getParent() != null) {
                WindowManager wm = (WindowManager) App.getInstance().getSystemService(Context.WINDOW_SERVICE);
                wm.removeViewImmediate(emojiView);
            }
        } catch (Exception e) {
            Log.e(Constants.TAG, e.getMessage());
        }
    }



    */
/**
     * Hides the emoji popup
     *//*

    public void hideEmojiPopup() {
        if (showingEmoji) {
            showEmojiPopup(false);
        }
    }

    */
/**
     * Check if the emoji popup is showing
     *
     * @return
     *//*

    public boolean isEmojiPopupShowing() {
        return showingEmoji;
    }

    */
/**
     * Updates emoji views when they are complete loading
     *
     * @param id
     * @param args
     *//*

    @Override
    public void didReceivedNotification(int id, Object... args) {
        if (id == NotificationCenter.emojiDidLoaded) {
            if (emojiView != null) {
                emojiView.invalidateViews();
            }

            if (chatListView != null) {
                chatListView.invalidateViews();
            }
        }
    }

 */
/*   @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public Voice voicecheck()
    {
        Set<Voice> _voiceName = t1.getVoices();

        for (Voice tmpVoice : t1.getVoices()) {
            if (tmpVoice.getName().equals(_voiceName)) {
                return tmpVoice;

            }
            else
            {
                return null;
            }
        }
        return null;
    }
 *//*

 private void installVoiceData() {
     Intent intent = new Intent(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
     intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
     intent.setPackage("com.google.android.tts"*/
/*replace with the package name of the target TTS engine*//*
);
     try {
         Log.v("Hello", "Installing voice data: " + intent.toUri(0));
         startActivity(intent);
     } catch (ActivityNotFoundException ex) {
         Log.e("Hello", "Failed to install TTS data, no acitivty found for " + intent + ")");
     }
 }
 @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onInit(int i) {


     if (i != TextToSpeech.ERROR) {

        // t1.setLanguage(locselection);


        // t1.setLanguage(Locale.ENGLISH);
        // t1.setLanguage(Locale.CANADA_FRENCH);
         //t1.isLanguageAvailable(Locale.US);
         // installVoiceData();
         //t1.addSpeech("Hello", "com.google.android.tts", R.raw.tv_enua);
         // t1.getVoices();
         // t1.setEngineByPackageName("com.google.android.tts");
         // t1.getDefaultVoice();


     }

    }

    @Override
    public void onReadyForSpeech(Bundle bundle) {
        Toast.makeText(this, "Ready for speech", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBeginningOfSpeech() {

    }

    @Override
    public void onRmsChanged(float v) {

    }

    @Override
    public void onBufferReceived(byte[] bytes) {

    }

    @Override
    public void onEndOfSpeech() {


    }

    @Override
    public void onError(int i) {

        Toast.makeText(this,getErrorText(i), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onResults(Bundle results) {

        ArrayList<String> matches = results
                .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        chatEditText1.setText(matches.get(0));
        chatEditText1.setVisibility(View.INVISIBLE);
        sendMessage(chatEditText1.getText().toString(), UserType.OTHER);
        chatEditText1.setText("");
        chatEditText1.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPartialResults(Bundle bundle) {

    }

    @Override
    public void onEvent(int i, Bundle bundle) {

    }

    public String getErrorText(int errorCode) {
        String message;
        switch (errorCode) {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";

                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "Client side error";
                break;
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "Network error";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "No match !! Please try again";
                speechsynthesis();
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "No speech input";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }

}
*/
