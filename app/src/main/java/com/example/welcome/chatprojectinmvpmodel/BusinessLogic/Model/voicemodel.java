package com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model;

import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;

import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.voiceview;

import java.util.ArrayList;


/**
 * Created by welcome on 20-09-2017.
 */

public class voicemodel implements TextToSpeech.OnInitListener,RecognitionListener {

    voiceview voicevi;

    public voicemodel(voiceview voice)
    {
        this.voicevi = voice;
    }
    public voicemodel()
    {

    }

    @Override
    public void onReadyForSpeech(Bundle bundle) {

        voicevi.begining();

    }

    @Override
    public void onBeginningOfSpeech() {

    }

    @Override
    public void onRmsChanged(float v) {

    }

    @Override
    public void onBufferReceived(byte[] bytes) {

    }

    @Override
    public void onEndOfSpeech() {

    }

    @Override
    public void onError(int i) {

        voicevi.error();

    }

    @Override
    public void onResults(Bundle results) {

        ArrayList<String> matches = results
                .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        voicevi.success(matches.get(0));
       // chatEditText1.setText(matches.get(0));
       // chatEditText1.setVisibility(View.INVISIBLE);
       // sendMessage(chatEditText1.getText().toString(), UserType.OTHER);

    }

    @Override
    public void onPartialResults(Bundle bundle) {

    }

    @Override
    public void onEvent(int i, Bundle bundle) {

    }

    @Override
    public void onInit(int i) {

        if (i != TextToSpeech.ERROR) {
            voicevi.speakout();
        }

    }
}
