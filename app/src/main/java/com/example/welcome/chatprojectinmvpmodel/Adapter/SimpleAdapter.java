package com.example.welcome.chatprojectinmvpmodel.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.welcome.chatprojectinmvpmodel.R;

import java.util.List;


/**
 * Created by Venkatesh on 26-09-2017.
 */

public class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.MyViewHolder> {

    private List<String> list_item ;
    private Context mcontext;



    public SimpleAdapter(List<String> list, Context context) {

        list_item = list;
        mcontext = context;
    }

    // Called when RecyclerView needs a new RecyclerView.ViewHolder of the given type to represent an item.
    @Override
    public SimpleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a layout
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.chat_user2_item, null);

        return new MyViewHolder(view);
    }


    // Called by RecyclerView to display the data at the specified position.
    @Override
    public void onBindViewHolder(final MyViewHolder viewHolder, final int position ) {



        viewHolder.textviewdate.setText(seperation(list_item.get(position)));


        viewHolder.country_name.setText(list_item.get(position));



        viewHolder.country_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(mcontext, list_item.get(position),
                        Toast.LENGTH_LONG).show();
            }
        });

    }

    private String seperation(String result) {
        String[] array = result.split(":");
        String verified = "";
        for (String anArray : array) {
            String[] innerData = anArray.split(":");
            verified = innerData[0];


        }
        return verified;
    }

    // initializes textview in this class
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView country_name,textviewdate,textviewtime;

        MyViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            country_name = (TextView) itemLayoutView.findViewById(R.id.textview_message);
            //textviewdate = (TextView) itemLayoutView.findViewById(R.id.textview_date);
            //textviewtime = (TextView) itemLayoutView.findViewById(R.id.textview_time);
        }
    }

    //Returns the total number of items in the data set hold by the adapter.
    @Override
    public int getItemCount() {
        return list_item.size();
    }

}
