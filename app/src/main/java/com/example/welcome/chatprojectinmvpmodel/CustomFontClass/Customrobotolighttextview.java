package com.example.welcome.chatprojectinmvpmodel.CustomFontClass;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.example.welcome.chatprojectinmvpmodel.R;


/**
 * Created by VENKATESH on 2/27/2017.
 */

public class Customrobotolighttextview extends TextView {
    Typeface csTypeFace = null;

    private final Paint mPaint = new Paint();

    private final Rect mBounds = new Rect();
    public Customrobotolighttextview(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        csTypeFace = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.robotolight));

        setCustomDesign();
    }

    public Customrobotolighttextview(Context context)
    {
        super(context);

        csTypeFace = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.robotolight));

        setCustomDesign();
    }


    public void setCustomDesign()
    {
        Customrobotolighttextview.this.setTypeface(csTypeFace);
    }

    public void setCsTypeFace(Typeface csTypeFace) {
        this.csTypeFace = csTypeFace;
    }

}
