package com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Model;


import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Presenter.registerpresenter;
import com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View.registerview;
import com.example.welcome.chatprojectinmvpmodel.Others.Constants;

import static com.example.welcome.chatprojectinmvpmodel.Others.Constants.PASSWORDWITHLENGTHERROR;
import static com.example.welcome.chatprojectinmvpmodel.Others.Constants.VALIDEMAILERROR;
import static com.example.welcome.chatprojectinmvpmodel.Others.Constants.VALIDPASSWORDERROR;

/**
 * Created by welcome on 21-09-2017.
 */

public class registermodel implements registerpresenter {

    private registerview mregisterview;

    public registermodel(registerview mregisterview)
    {
        this.mregisterview = mregisterview;
    }


    @Override
    public void customerdetailsforregistering(String username, String useremail, String userpassword) {
        
        if(useremailvalidation(useremail))
        {
            if (usernamevalidation(username))
            {
                if(userpasswordvalidation(userpassword))
                {
                    mregisterview.registerofflinevalidationsucess(username,useremail,userpassword);
                }
            }
        }

    }

    private Boolean usernamevalidation(String username) {


        if (username.isEmpty()) {
            mregisterview.registerationfailure(Constants.VALIDNAMEERROR);
            return false;
        } else if (username.length() <= 4) {
            mregisterview.registerationfailure(Constants.VALIDNAMEERROR);
            return false;
        } else {
            return true;
        }


    }

    private Boolean userpasswordvalidation(String password) {


        if (password.isEmpty()) {
            mregisterview.registerationfailure(VALIDPASSWORDERROR);
            return false;
        } else if (!(password.length() >= 6)) {
            mregisterview.registerationfailure(PASSWORDWITHLENGTHERROR);
            return false;
        } else {
            return true;
        }


    }

    private Boolean useremailvalidation(String useremail)
    {
        if (useremail.isEmpty()) {
            mregisterview.registerationfailure(VALIDEMAILERROR);
            return false;
        } else if (!isValidEmailAddress(useremail)) {
            mregisterview.registerationfailure(VALIDEMAILERROR);
            return false;
        } else {
            
            return true;
        }

    }

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }


}
