package com.example.welcome.chatprojectinmvpmodel.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.welcome.chatprojectinmvpmodel.R;

/**
 * Created by Venkatesh on 27-10-2017.
 */

public class Serviceadapter extends RecyclerView.Adapter {

    Context context;
    String[] Services = {"Searching of Flights", "Searching of Restaurants","Cancellation"};
    int resid[] = {R.drawable.ic_airplane, R.drawable.icons8restaurant,R.drawable.ic_ticket};

    public Serviceadapter(Context context) {

        this.context = context;


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.homeservicelayout, parent, false);

        return (Serviceadapterholder) new Serviceadapterholder(row);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ((Serviceadapterholder) holder).filghtssearch.setText(Services[position]);
        ((Serviceadapterholder)holder).imagesearch.setImageResource(resid[position]);



    }

    @Override
    public int getItemCount() {
        return Services.length;
    }

    public class Serviceadapterholder extends RecyclerView.ViewHolder {

        TextView filghtssearch;
        ImageView imagesearch;

        public Serviceadapterholder(View itemView) {

            super(itemView);
            filghtssearch = (TextView) itemView.findViewById(R.id.servicessearch);
            imagesearch = (ImageView) itemView.findViewById(R.id.imagesearch);


        }
    }
}
