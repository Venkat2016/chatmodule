package com.example.welcome.chatprojectinmvpmodel.Others;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.example.welcome.chatprojectinmvpmodel.Models.ChatMessage;
import com.example.welcome.chatprojectinmvpmodel.Models.Status;
import com.example.welcome.chatprojectinmvpmodel.Models.UserType;
import com.example.welcome.chatprojectinmvpmodel.sqlitefiles.Myopenhelper;

import java.util.ArrayList;

import static com.example.welcome.chatprojectinmvpmodel.sqlitefiles.Myopenhelper.DICTIONARY_TABLE_NAME;

/**
 * Created by welcome on 21-09-2017.
 */


public class Backgroundthread extends AsyncTask<Object, Object, Long> {

    private static final String KEY_WORD = "Admin";
    private static final String KEY_DEFINITION = "Chatuser";
    private static final String KEY_DATE = "DATE";
    private static final String KEY_TIME = "TIME";
    private static final String KEY_STATUS = "STATUS";
    ArrayList<ChatMessage> arrayList;

    Context context;
    private long result;


    public Backgroundthread(Context context, ArrayList<ChatMessage> arrayList) {
        this.context = context;
        this.arrayList = arrayList;


    }

    @Override
    protected void onPreExecute() {

        super.onPreExecute();
    }

    @Override
    protected Long doInBackground(Object... voids) {

        Myopenhelper myopenhelper = new Myopenhelper(context);
        SQLiteDatabase db = myopenhelper.getWritableDatabase();


        for (int i = 0; i < arrayList.size(); i++) {
            ContentValues contentValues = new ContentValues();

            if (arrayList.get(i).getUserType() == UserType.COOL) {
                contentValues.put(KEY_WORD, arrayList.get(i).getMessageText());
                contentValues.put(KEY_DATE, arrayList.get(i).getMessagedate());
                contentValues.put(KEY_TIME, arrayList.get(i).getMessageTime());
                if (arrayList.get(i).getMessageStatus() == "READ") {
                    contentValues.put(KEY_STATUS, "Delivered");
                } else {
                    contentValues.put(KEY_STATUS, "Not Delivered");
                }

            } else if (arrayList.get(i).getUserType() == UserType.OTHER) {
                contentValues.put(KEY_DEFINITION, arrayList.get(i).getMessageText());
                contentValues.put(KEY_DATE, arrayList.get(i).getMessagedate());
                contentValues.put(KEY_TIME, arrayList.get(i).getMessageTime());

                if (arrayList.get(i).getMessageStatus() == "READ") {
                    contentValues.put(KEY_STATUS, "Delivered");
                } else {
                    contentValues.put(KEY_STATUS, "Not Delivered");
                }


            } else if (arrayList.get(i).getUserType() == UserType.MULTIOPTIONS) {
                contentValues.put(KEY_DEFINITION, arrayList.get(i).getMessageText());
                contentValues.put(KEY_DATE, arrayList.get(i).getMessagedate());
                contentValues.put(KEY_TIME, arrayList.get(i).getMessageTime());


            }

            result = db.insertOrThrow(DICTIONARY_TABLE_NAME, null, contentValues);

        }

        Log.i("Checking", result + "");


        return result;
    }

    @Override
    protected void onPostExecute(Long aLong) {

        super.onPostExecute(aLong);
    }
}

