package com.example.welcome.chatprojectinmvpmodel.Models;

import org.apache.http.NameValuePair;

/**
 * Created by Venkatesh on 28-11-2017.
 */

public class Chatsending implements NameValuePair {
    private String clientid;

    public String getClientid() {
        return clientid;
    }

    public void setClientid(String clientid) {
        this.clientid = clientid;
    }

    public String getSessionkey() {
        return sessionkey;
    }

    public void setSessionkey(String sessionkey) {
        this.sessionkey = sessionkey;
    }

    public int getMsg_status() {
        return msg_status;
    }

    public void setMsg_status(int msg_status) {
        this.msg_status = msg_status;
    }

    public int getMsg_flow() {
        return msg_flow;
    }

    public void setMsg_flow(int msg_flow) {
        this.msg_flow = msg_flow;
    }

    public String getMsg_text() {
        return msg_text;
    }

    public void setMsg_text(String msg_text) {
        this.msg_text = msg_text;
    }

    private String sessionkey;
    private int msg_status;
    private int msg_flow;
    private String msg_text;

    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getValue() {
        return null;
    }
}
