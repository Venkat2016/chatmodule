package com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View;


import com.example.welcome.chatprojectinmvpmodel.Models.ChatMessage;

import java.util.ArrayList;

/**
 * Created by welcome on 14-09-2017.
 */

public interface chatview {

   // void sendmessage(String message);

    void sendmessage(ChatMessage chatMessage);

    void successstore(ArrayList<ChatMessage> chatMessage);

    void end(String text);

    void retry(String text);

    void progressdisplayon();

    void progressdisplayoff();

    void edittextdisable();

    void manualtransfer();

    //  void senttonetwork(ChatMessage chatMessage);

}
