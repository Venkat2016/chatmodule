package com.example.welcome.chatprojectinmvpmodel.BusinessLogic.View;

/**
 * Created by welcome on 21-09-2017.
 */

public interface registerview {


    void registerofflinevalidationsucess(String username, String useremail, String password);

    void registersuccess();

    void registerationfailure(String message);

    void networkfailure(String message);

    void showprogressview();

    void hideprogressview();


}
