package com.example.welcome.chatprojectinmvpmodel.Activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ListView;

import com.example.welcome.chatprojectinmvpmodel.R;
import com.mapzen.speakerbox.Speakerbox;

import java.util.Locale;
import java.util.Set;


public class Languagelisted extends Activity {

    ListView listview_language;
    private Speakerbox speakerbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_languagelisted);



        // Test calling play() immediately (before TTS initialization is complete).
       // speakerbox.play(textView.getText());

        listview_language = (ListView) findViewById(R.id.listview_language);
        getAvailableLanguasges();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void getAvailableLanguasges() {
        Set<Locale> availableLanguages = speakerbox.getTextToSpeech().getAvailableLanguages();

       // languagesLayout.removeAllViews();

        LayoutInflater inflater = LayoutInflater.from(this);
        for (final Locale locale : availableLanguages) {

            Log.i("Check", locale.getDisplayName());
/*
            View view = inflater.inflate(R.layout.language_row, null, false);
            TextView textView = (TextView) view.findViewById(R.id.text_view);
            textView.setText(locale.getDisplayName());
*/

/*
            Button button = (Button) view.findViewById(R.id.btn);
            button.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    speakerbox.setLanguage(locale);
                }
            });
*/

           // languagesLayout.addView(view);
        }
    }

}
