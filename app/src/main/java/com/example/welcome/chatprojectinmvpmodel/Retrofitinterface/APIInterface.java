package com.example.welcome.chatprojectinmvpmodel.Retrofitinterface;

import com.google.gson.JsonObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by welcome on 12-09-2017.
 */

public interface APIInterface {


    @POST("next")
    @Headers({
            "Accept-Encoding: identity",
            "Content-Type: application/json",
            "Content-Length:128"
    })
    Call<ResponseBody> getpythonnext(@Body JsonObject jsonObject);


    @POST("newcall")
    @Headers({
            "Accept-Encoding: identity",
            "Content-Type: application/json",
            "Content-Length:128"
    })
    Call<ResponseBody> getpython(@Body JsonObject jsonObject);

    @POST("queue")
    @Headers({
            "Accept-Encoding: identity",
            "Content-Type: application/json",
            "Content-Length:128"
    })
    Call<ResponseBody> getpythonqueue(@Body JsonObject jsonObject);

    @FormUrlEncoded
    @POST("/api/user_agent_message")
    Call<ResponseBody> getnormalmongo(@Field("msg_agent_id") String agentid,
                                      @Field("msg_flow") String msgflow,
                                      @Field("msg_session_id") String msgsesid,
                                      @Field("msg_text") String msg_text,
                                      @Field("msg_status") String msg_status);


    @FormUrlEncoded
    @POST("/auth/local")
    Call<ResponseBody> signinapi(@Field("email") String email,
                                 @Field("password") String password);


}
