package com.example.welcome.chatprojectinmvpmodel.EventBusClasses;

/**
 * Created by welcome on 21-09-2017.
 */

public class Customloginresponsemessage {
    private String usermobile;
    private String useremail;
    private String username;


    public String getUsermobile() {
        return usermobile;
    }

    public void setUsermobile(String usermobile) {
        this.usermobile = usermobile;
    }

    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }





}
