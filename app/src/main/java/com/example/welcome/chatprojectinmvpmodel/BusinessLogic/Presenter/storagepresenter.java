package com.example.welcome.chatprojectinmvpmodel.BusinessLogic.Presenter;

import android.content.Context;

import com.example.welcome.chatprojectinmvpmodel.Models.ChatMessage;

import java.util.ArrayList;


/**
 * Created by welcome on 23-09-2017.
 */

public interface storagepresenter {

    void sendvaluesupdated(Context context, ArrayList<ChatMessage> chatmessage);

    void getvaluesfromdb(Context context);



}
